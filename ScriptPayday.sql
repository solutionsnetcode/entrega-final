-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------
-- Dumping database structure for paydaybd
CREATE DATABASE IF NOT EXISTS `paydaybd` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `paydaybd`;

-- Dumping structure for table paydaybd.auditoria
CREATE TABLE IF NOT EXISTS `auditoria` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdUsuario` int(10) unsigned DEFAULT NULL,
  `Descripcion` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.auditoria: ~0 rows (approximately)
/*!40000 ALTER TABLE `auditoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `auditoria` ENABLE KEYS */;

-- Dumping structure for table paydaybd.canjeusuariosproductos
CREATE TABLE IF NOT EXISTS `canjeusuariosproductos` (
  `IdCanje` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdUsuario` int(10) unsigned NOT NULL,
  `IdProductoCatalogo` int(10) unsigned NOT NULL,
  `FechaRealizado` date NOT NULL,
  `Anulado` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IdCanje`),
  KEY `FK_Usuario_CanjeProducto` (`IdUsuario`),
  KEY `FK_Producto_CanjeProducto` (`IdProductoCatalogo`),
  CONSTRAINT `FK_Producto_CanjeProducto` FOREIGN KEY (`IdProductoCatalogo`) REFERENCES `productoscatalogo` (`IdProductoCatalogo`),
  CONSTRAINT `FK_Usuario_CanjeProducto` FOREIGN KEY (`IdUsuario`) REFERENCES `usuarios` (`IdUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.canjeusuariosproductos: ~0 rows (approximately)
/*!40000 ALTER TABLE `canjeusuariosproductos` DISABLE KEYS */;
/*!40000 ALTER TABLE `canjeusuariosproductos` ENABLE KEYS */;

-- Dumping structure for table paydaybd.mediosdepago
CREATE TABLE IF NOT EXISTS `mediosdepago` (
  `IdMedioPago` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Descripcion` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UrlPago` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Activo` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IdMedioPago`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.mediosdepago: ~0 rows (approximately)
/*!40000 ALTER TABLE `mediosdepago` DISABLE KEYS */;
INSERT INTO `mediosdepago` (`IdMedioPago`, `Nombre`, `Descripcion`, `UrlPago`, `Activo`, `created_at`, `updated_at`) VALUES
	(1, 'Visa', 'Tarjetas Visas', 'https://www.visanet.com.uy/', 1, '2020-09-28 04:51:25', '2020-09-28 04:51:25');
/*!40000 ALTER TABLE `mediosdepago` ENABLE KEYS */;

-- Dumping structure for table paydaybd.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.migrations: ~12 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2020_05_22_015013_crear_tabla__tipos_usuario', 1),
	(2, '2020_05_22_015105_crear_tabla__roles', 1),
	(3, '2020_05_22_015106_crear_tabla_monedas', 1),
	(4, '2020_05_22_021245_crear_tabla__usuarios', 1),
	(5, '2020_05_22_021338_crear_tabla__personas_fisicas', 1),
	(6, '2020_05_22_022145_crear_tabla__personas_juridicas', 1),
	(7, '2020_05_22_022205_crear_tabla__personas_empleados', 1),
	(8, '2020_05_22_022326_crear_tabla_telefonos_usuarios', 1),
	(9, '2020_05_22_022458_crear_tabla__productos_catalogo', 1),
	(10, '2020_05_22_022459_crear_tabla_canjeu_suarios_productos', 1),
	(11, '2020_05_23_183347_crear_tabla__medios__de__pago', 1),
	(12, '2020_05_23_183649_crear_tabla_servicios', 1),
	(13, '2020_05_23_225310_crear_tabla_pagos', 1),
	(14, '2020_05_23_225643_crear_tabla_publicidades', 1),
	(15, '2020_05_24_045224_crear_tabla_permisos', 1),
	(16, '2020_05_31_213406_crear_tabla_serviciosagendados', 1),
	(17, '2020_05_31_213747_crear_tabla_tarjetas', 1),
	(18, '2020_05_31_233152_crear_tabla_permisos_roles', 1),
	(19, '2020_09_15_000730_crear_tabla_auditoria', 1),
	(20, '2014_10_12_100000_create_password_resets_table', 2),
	(21, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
	(22, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
	(23, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
	(24, '2016_06_01_000004_create_oauth_clients_table', 2),
	(25, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table paydaybd.monedas
CREATE TABLE IF NOT EXISTS `monedas` (
  `IdMoneda` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Simbolo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Activo` tinyint(1) NOT NULL,
  `PorcentajePuntos` decimal(4,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IdMoneda`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.monedas: ~2 rows (approximately)
/*!40000 ALTER TABLE `monedas` DISABLE KEYS */;
INSERT INTO `monedas` (`IdMoneda`, `Nombre`, `Simbolo`, `Activo`, `PorcentajePuntos`, `created_at`, `updated_at`) VALUES
	(1, 'Pesos Uruguayos', '$', 1, 0.01, '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(2, 'Dolar Estadounidense', 'U$S', 1, 0.50, '2020-09-26 21:20:01', '2020-09-26 21:20:01');
/*!40000 ALTER TABLE `monedas` ENABLE KEYS */;

-- Dumping structure for table paydaybd.oauth_access_tokens
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.oauth_access_tokens: ~49 rows (approximately)
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
	('05709a20da420327a8a89fd4fd7ed04b1f56b0b991ddcd619f738376920432b28005f754a9b665ac', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:56:59', '2020-09-28 15:56:59', '2021-09-28 15:56:59'),
	('0a14e8a6f744727d2703405f7e87d333bd74aae92043acef96df1d09181c79eacf1387f733ca60b5', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 13:41:11', '2020-09-28 13:41:11', '2021-09-28 13:41:11'),
	('0b2f64c1e4fc8642aca5a2e2f8ebc36ec3156371a74bb604d505366b3c6400ebbd1e1864165d1b27', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:36:07', '2020-09-28 15:36:07', '2021-09-28 15:36:07'),
	('10def9b9abb7a54a60f9b1d39575567d986c0462bcac51b19e9a68d397c0e3f331a9abe744b73ae7', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:36:27', '2020-09-28 15:36:27', '2021-09-28 15:36:27'),
	('124ea327cf2ab94ec8fd2253100244dcc67c6d8a533174223c0ffa390017b1ee3c6da8c733827bb6', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 07:01:26', '2020-09-28 07:01:26', '2021-09-28 07:01:26'),
	('160bc2c963b7e5db17a9e763a5868b6f9740c545c6b0a83d372e3a7e1bb4bc0938e393a42ac8cd13', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 06:58:51', '2020-09-28 06:58:51', '2021-09-28 06:58:51'),
	('18fde080c60c9f0a496c76951f29f8c39541060dad68dbcc8f9aded4e2d86c7cb0660c5e901b46ff', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:01:37', '2020-09-28 15:01:37', '2021-09-28 15:01:37'),
	('1c0beda493b64e6712c54c2d177560c615b42361341d38f348f14df5ff8c93624dc12959017df6ac', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 13:59:57', '2020-09-28 13:59:57', '2021-09-28 13:59:57'),
	('1d843d9d1c177b7d85be6e4016eb4eff7adec664018515fe96393e62a00b51a267c4e90b77dc6b47', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 14:01:25', '2020-09-28 14:01:25', '2021-09-28 14:01:25'),
	('26696ff5e20636e3d1e28cdcd0cbea625db2075bf48af65a7325307f950c443fc8d3cd91976b709c', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 22:52:13', '2020-09-28 22:52:13', '2021-09-28 22:52:13'),
	('26a9731d31d27301475052a2a55e7df0a66d8d01d566e0daa5f900352944a586ad92849748b6b8d4', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 07:04:30', '2020-09-28 07:04:30', '2021-09-28 07:04:30'),
	('332d1e2de5849760db8ce1a1805f8ab17b978c02bf1ed5e27e0873aa53faacca756fcf492822152b', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 06:44:44', '2020-09-28 06:44:44', '2021-09-28 06:44:44'),
	('38111af47d1f351960c92a2c27dd626776e61d45e4135c24b502bafc34c384593a0412b87948daf8', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 17:50:03', '2020-09-28 17:50:03', '2021-09-28 17:50:03'),
	('3b470091d4d356a6e5c16df378b09b88cc70a4ba6db83a37ea5ed91a8b6fb4768912637478819722', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 13:38:30', '2020-09-28 13:38:30', '2021-09-28 13:38:30'),
	('529b160a80b1ae1f72f545207eba8bdce535a1c985c5d42ef14fc4a71e5f14a671b2917809684406', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 22:52:37', '2020-09-28 22:52:37', '2021-09-28 22:52:37'),
	('52b61041dcff58f13414383b277978d0e62fb382604c8b9e25f664490607fb8ae6330affd953cdbf', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:52:01', '2020-09-28 15:52:01', '2021-09-28 15:52:01'),
	('5422e3c7236a38e51962237112a4af16e91a5847a9843fce828a415df0ada8c767557bf4f58f0b26', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:49:18', '2020-09-28 15:49:18', '2021-09-28 15:49:18'),
	('5b94731c4787037c7cfacbd29cfe040c4bd5775b99f86e82b8c4d3c9c2d2bc955a0919924a9ecf72', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 13:40:12', '2020-09-28 13:40:12', '2021-09-28 13:40:12'),
	('63322f3aea2a3a9aef8774ba60509752baafe0ec5b86320a190a0f543488ec6aa9c6f70fc25bacb1', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 14:27:47', '2020-09-28 14:27:47', '2021-09-28 14:27:47'),
	('65dc13dbc76e9231218a47295be98ee1e0dab394f80246ece13b117a56dbb617f40e425fc6dac766', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 14:28:47', '2020-09-28 14:28:47', '2021-09-28 14:28:47'),
	('682e213e3d0b7fb842b7cf1de8f82eeac8bc4b97fae0ccd367e69a216d75b7b423ee1ede0a6a0f49', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:22:25', '2020-09-28 15:22:25', '2021-09-28 15:22:25'),
	('6e8893d58880f26860ca6bc65c924b6c7f1a3a300ef7770e74648ed92d458ea3b9bd34c70aa54587', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:52:57', '2020-09-28 15:52:57', '2021-09-28 15:52:57'),
	('71b08e08cbb8d87b9705d51b2547797b154d011b7cfc879d336e2494484a9e060c22423b74f23888', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 13:57:40', '2020-09-28 13:57:40', '2021-09-28 13:57:40'),
	('74064037e487d197d865844d760f9f551be53ca3d7477a3249a6eed9e92d06923d0b58577bb09e78', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 14:00:56', '2020-09-28 14:00:56', '2021-09-28 14:00:56'),
	('793c3ce6d40c1d7c9185ccd59a93d1d4de136756f4d26a25bbb0e558e569a6290fc467495a9e9643', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 07:02:48', '2020-09-28 07:02:48', '2021-09-28 07:02:48'),
	('7be21ee65f6ed260df7545d597670fecc5c081923d751e9c72c589aabf469ffd873ef067a471624c', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 14:19:10', '2020-09-28 14:19:10', '2021-09-28 14:19:10'),
	('7cdb68c1396278c319d2e25419eebce5ae7a1e0e765596044cec8c846eb39301ba596f9653f60117', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:37:31', '2020-09-28 15:37:31', '2021-09-28 15:37:31'),
	('801ed1211a418e46c65221932735f387db9f3102e84afaee4fe88a127365c86bc396d0d0a16e1f88', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:54:29', '2020-09-28 15:54:29', '2021-09-28 15:54:29'),
	('8757ebef33f88bf8949d2e043d42a5c6e3da702968b2d8286d82d2a0d0da4dd8e0f8d3810a95fd22', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 23:22:13', '2020-09-28 23:22:13', '2021-09-28 23:22:13'),
	('87b05df00e6dd2592da12214424bebd2c6934edec9e9582d7f7acfb85d0ff125acd7fad84ec5c86b', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 13:57:00', '2020-09-28 13:57:00', '2021-09-28 13:57:00'),
	('8840eb2490f4af6065e6246459f93a0265cf65f77310b4a61f3a007b97f82ca809a6d31757311765', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 23:41:38', '2020-09-28 23:41:38', '2021-09-28 23:41:38'),
	('931f2c425b6a82f8b849f034601d534c4c696b9533a7c4d3d8d6c01bfee0d0133a4c8ccfbefe5c02', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:17:58', '2020-09-28 15:17:58', '2021-09-28 15:17:58'),
	('93d553d43030b1a45932e377b85bd8671ed0b71da7a1adb977bf371259b561380ce372493ca894bc', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 14:00:48', '2020-09-28 14:00:48', '2021-09-28 14:00:48'),
	('940ae70ede75a29f344e58fe89455dfb1401bae2ecadc1158b35eb4fe6954080fd16911026e9323d', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:29:00', '2020-09-28 15:29:00', '2021-09-28 15:29:00'),
	('9fbbab510694a6aecabbd08dacd7f8b91abf87eb2cf8cbe72a6c26de8ce5731ed649557b09c72997', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 13:42:17', '2020-09-28 13:42:17', '2021-09-28 13:42:17'),
	('a38d5e37a456835e0c96bd530577af07da6772be27c6850de949fe7252f21e794edd2ffe7549d762', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 06:52:35', '2020-09-28 06:52:35', '2021-09-28 06:52:35'),
	('b7483a267553584e858e8ba8d60496c47fb674d632161bf274a75da6f5ded03e4ac95b331ac936c4', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:10:28', '2020-09-28 15:10:28', '2021-09-28 15:10:28'),
	('bb0dc4af02c8dea5790a5a49fac5f417444dd9bd34c241e3baf7c76f89fcceb22c344b9f2167100c', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 13:55:23', '2020-09-28 13:55:23', '2021-09-28 13:55:23'),
	('c1937c9f4a98f43870c397f033ba2227a1a7b3da07750d9a4875deedd921b858650eb6497dfd0be0', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 23:09:38', '2020-09-28 23:09:38', '2021-09-28 23:09:38'),
	('c2916189d9bd70e0724afb01f2331559a44d79a0c379a702c7d0ed370b1af67e06b20ecdc78b93c3', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:24:09', '2020-09-28 15:24:09', '2021-09-28 15:24:09'),
	('c4e8d013572db12310cb6985be7b8b8728827103ce219a614cdb62074de5ac39e2709a97c6455888', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 23:43:27', '2020-09-28 23:43:27', '2021-09-28 23:43:27'),
	('c910ebb315adb721e96996e6764c0afde63b48370b69b8b09468f97f03745fd51dc29bfd02abfe89', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 07:01:08', '2020-09-28 07:01:08', '2021-09-28 07:01:08'),
	('d5a298d8441557ed3952cca695fc9fa73deb2e2cc12f0ba0c3b728b2c1613cb629331b4d30d51fdb', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 21:18:05', '2020-09-28 21:18:05', '2021-09-28 21:18:05'),
	('d8ed09fff5c118a6c61c58f382e575fb6860a665831d3e0ef30986318e86892f339d59d25e718fc5', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 17:57:16', '2020-09-28 17:57:16', '2021-09-28 17:57:16'),
	('dcec869397b26be93a21c1340a0e84b870f9cd08395d84fccb762962af2a819ea587e4f3c2e1327d', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 14:02:26', '2020-09-28 14:02:26', '2021-09-28 14:02:26'),
	('dea4871d49bfcda77f0c8d0e9cf643f52241288ce7ac90e149cd5d8f24e072400d1953ab12dff488', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-29 01:14:44', '2020-09-29 01:14:44', '2021-09-29 01:14:44'),
	('dec8f3d4319dc0a25fea5a5c6bf97e8f7c7dcd22b07233945056f9e89ad6883043c39c7c07d64eb1', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:25:28', '2020-09-28 15:25:28', '2021-09-28 15:25:28'),
	('e25fdf9e65b900f5a6352fe098a0067c9fe680b6974080f4ab5718ea6a4a51261b5584a2a0e17b0c', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 14:21:54', '2020-09-28 14:21:54', '2021-09-28 14:21:54'),
	('e5944c7d7ffb1faf32f6ce7a9d8c9d18f45eb9b6006e4240aea1cd6d749165fbf55f63a1a7fc4ca5', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 15:23:26', '2020-09-28 15:23:26', '2021-09-28 15:23:26'),
	('ea13d4a79e32646a09a8de763364566b002c951f42a04671edf8fe3d8dd7ac4cbb995a44d71302c1', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 06:43:57', '2020-09-28 06:43:57', '2021-09-28 06:43:57'),
	('ebfe2fa18c23635cb93c378c17a4ab0f704387af03c88d3c1ad50d81b55119775801042c844cbfdd', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 18:03:18', '2020-09-28 18:03:18', '2021-09-28 18:03:18'),
	('edee42973fa456dcdc497b6b18f895261e64294c0f6e87cf6e824202863e7739a1d9ea5a48d7a71c', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 14:15:11', '2020-09-28 14:15:11', '2021-09-28 14:15:11'),
	('f1b6681446223e12440e3c06fae9858fed5ae5b2253b901150b87537233377ff266376770b677c91', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 06:57:31', '2020-09-28 06:57:31', '2021-09-28 06:57:31'),
	('f4c9d41a7f6489b60da02e1d555fcedfff00909e163f2532d2b4ba9d4756bddbe32b7ef6bfd9959d', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 13:34:14', '2020-09-28 13:34:14', '2021-09-28 13:34:14'),
	('f8e9bbe570f52e0874405f0818704107144327f1cd490f7f982821753ea47d7d0037843ff81c5af7', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 13:41:44', '2020-09-28 13:41:44', '2021-09-28 13:41:44'),
	('f907f8ec0d35c72a6c6f69d142f39e11b4de075383cf48bfaabaa2a0979ebb42d1d2515d7250ecdc', 2, 6, 'Personal Access Token', '[]', 0, '2020-09-28 07:00:40', '2020-09-28 07:00:40', '2021-09-28 07:00:40');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Dumping structure for table paydaybd.oauth_auth_codes
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.oauth_auth_codes: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Dumping structure for table paydaybd.oauth_clients
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.oauth_clients: ~6 rows (approximately)
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Payday', 'zmkPw2THETUn2C6xqgdiz8zryT5C51yfK33hKKNj', NULL, 'yes', 0, 0, 0, '2020-09-28 06:42:14', '2020-09-28 06:42:14'),
	(2, NULL, 'Laravel Personal Access Client', 'hEO2Tamx22PNXpoZAjYHMoiPPQCa3TFTXHAFLK4C', NULL, 'http://localhost', 1, 0, 0, '2020-09-28 06:43:08', '2020-09-28 06:43:08'),
	(3, NULL, 'Laravel Password Grant Client', 'lOl2BAKUkJ6sVSjBbxSezzVdH47gGTkCAhoPDiZG', 'users', 'http://localhost', 0, 1, 0, '2020-09-28 06:43:08', '2020-09-28 06:43:08'),
	(4, NULL, 'Laravel Personal Access Client', 'zT1YTADUptYrEXKBWoPtd1pmuc1skJYTvxAtxzkP', NULL, 'http://localhost', 1, 0, 0, '2020-09-28 06:43:16', '2020-09-28 06:43:16'),
	(5, NULL, 'Laravel Password Grant Client', 'rLftp7UJ04GHiqOD0IWMiEOKNdJlIHwaWifH4riQ', 'users', 'http://localhost', 0, 1, 0, '2020-09-28 06:43:16', '2020-09-28 06:43:16'),
	(6, NULL, 'payday', 'Up5Rd7Lo030T6camvU4AAWyfVz4x07tUYC1ZG20n', NULL, 'http://localhost', 1, 0, 0, '2020-09-28 06:43:42', '2020-09-28 06:43:42');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Dumping structure for table paydaybd.oauth_personal_access_clients
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.oauth_personal_access_clients: ~3 rows (approximately)
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
	(1, 2, '2020-09-28 06:43:08', '2020-09-28 06:43:08'),
	(2, 4, '2020-09-28 06:43:16', '2020-09-28 06:43:16'),
	(3, 6, '2020-09-28 06:43:42', '2020-09-28 06:43:42');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Dumping structure for table paydaybd.oauth_refresh_tokens
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.oauth_refresh_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- Dumping structure for table paydaybd.pagos
CREATE TABLE IF NOT EXISTS `pagos` (
  `IdPago` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdUsuario` int(10) unsigned NOT NULL,
  `IdMoneda` tinyint(3) unsigned NOT NULL,
  `MontoPago` decimal(10,2) NOT NULL,
  `PuntosGenerados` int(10) unsigned NOT NULL DEFAULT '0',
  `IdServicio` int(10) unsigned NOT NULL,
  `FechaGenera` date NOT NULL,
  `FechaPaga` date NOT NULL,
  `IdMedioPago` tinyint(3) unsigned NOT NULL,
  `Anulado` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IdPago`),
  KEY `FK_Usuario_Pago` (`IdUsuario`),
  KEY `FK_Moneda_Pago` (`IdMoneda`),
  KEY `FK_Servicio_Pago` (`IdServicio`),
  KEY `FK_MedioPago_Pago` (`IdMedioPago`),
  CONSTRAINT `FK_MedioPago_Pago` FOREIGN KEY (`IdMedioPago`) REFERENCES `mediosdepago` (`IdMedioPago`),
  CONSTRAINT `FK_Moneda_Pago` FOREIGN KEY (`IdMoneda`) REFERENCES `monedas` (`IdMoneda`),
  CONSTRAINT `FK_Servicio_Pago` FOREIGN KEY (`IdServicio`) REFERENCES `servicios` (`IdServicio`),
  CONSTRAINT `FK_Usuario_Pago` FOREIGN KEY (`IdUsuario`) REFERENCES `usuarios` (`IdUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.pagos: ~3 rows (approximately)
/*!40000 ALTER TABLE `pagos` DISABLE KEYS */;
INSERT INTO `pagos` (`IdPago`, `IdUsuario`, `IdMoneda`, `MontoPago`, `PuntosGenerados`, `IdServicio`, `FechaGenera`, `FechaPaga`, `IdMedioPago`, `Anulado`, `created_at`, `updated_at`) VALUES
	(1, 2, 1, 500.00, 0, 1, '2020-09-28', '2020-09-28', 1, 0, NULL, NULL),
	(2, 2, 2, 100.00, 20, 2, '2020-09-24', '2020-09-25', 1, 0, NULL, NULL),
	(3, 2, 1, 299.00, 2, 3, '2020-09-29', '2020-09-29', 1, 0, NULL, NULL),
	(4, 2, 1, 500.00, 5, 1, '2020-09-29', '2020-09-29', 1, 0, '2020-09-29 00:08:02', '2020-09-29 00:08:02'),
	(5, 2, 1, 100000.00, 1000, 1, '2020-09-29', '2020-09-29', 1, 0, '2020-09-29 00:12:47', '2020-09-29 00:12:47'),
	(6, 2, 1, 50000.00, 500, 2, '2020-09-29', '2020-09-29', 1, 0, '2020-09-29 00:21:27', '2020-09-29 00:21:27'),
	(7, 2, 1, 50000.00, 500, 2, '2020-09-29', '2020-09-29', 1, 1, '2020-09-29 00:24:32', '2020-09-29 00:24:32'),
	(8, 2, 1, 50000.00, 500, 2, '2020-09-29', '2020-09-29', 1, 1, '2020-09-29 00:24:37', '2020-09-29 00:24:37'),
	(9, 2, 1, 50000.00, 500, 2, '2020-09-29', '2020-09-29', 1, 1, '2020-09-29 00:24:45', '2020-09-29 00:24:45'),
	(10, 2, 1, 50000.00, 500, 2, '2020-09-29', '2020-09-29', 1, 0, '2020-09-29 00:24:53', '2020-09-29 00:24:53'),
	(11, 2, 1, 200.00, 2, 1, '2020-09-29', '2020-09-29', 1, 1, '2020-09-29 00:30:19', '2020-09-29 00:30:19'),
	(12, 2, 1, 500.00, 5, 1, '2020-09-29', '2020-09-29', 1, 1, '2020-09-29 00:37:26', '2020-09-29 00:37:26'),
	(13, 2, 2, 9999999.00, 5000000, 3, '2020-09-29', '2020-09-29', 1, 1, '2020-09-29 01:15:03', '2020-09-29 01:15:03');
/*!40000 ALTER TABLE `pagos` ENABLE KEYS */;

-- Dumping structure for table paydaybd.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table paydaybd.permisos
CREATE TABLE IF NOT EXISTS `permisos` (
  `IdPermiso` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Permiso` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Uri` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IdPermiso`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.permisos: ~26 rows (approximately)
/*!40000 ALTER TABLE `permisos` DISABLE KEYS */;
INSERT INTO `permisos` (`IdPermiso`, `Permiso`, `Uri`, `created_at`, `updated_at`) VALUES
	(1, 'Alta Empleado', '/agregarEmpleado', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(2, 'Listar Empleado', '/listadoEmpleados', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(3, 'Editar Empleado', '/modificarEmpleado', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(4, 'Listar Clientes', '/listadoClientes', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(5, 'Modificar Clientes', '/', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(6, 'Listar Servicios', '/listadoServicios', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(7, 'Alta Servicio', '/ABMServiciosAjax', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(8, 'Modificar Servicio', '/ABMServiciosAjax', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(9, 'Listar Medio de Pago', '/listadoMetodos', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(10, 'Alta Medio de Pago', '/ABMMetodosDePago', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(11, 'Modificar Medio de Pago', '/ABMMetodosDePago', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(12, 'Listado de Pagos', '/listadoPagos', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(13, 'Listar Pagos', '/listadoPagos', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(14, 'Anular Pago', '/anularPago', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(15, 'Alta Rol', '/ABMRolAjax', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(16, 'Modificacion de Rol', '/ABMRolAjax', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(17, 'Listar Roles', '/listadoRoles', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(18, 'Lista de Publicidades', '/listadoPublicidad', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(19, 'Alta de Publicidad', '/altaPublicidad', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(20, 'Modificacion de Publicidad', '/bajaPublicidad', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(21, 'Listar Productos del Catalogo', '/listadoCatalogo', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(22, 'Alta Producto Catalogo', '/altaProductoCatalogo', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(23, 'Modificacion Producto Catalogo', '/modificacionProductoCatalogo', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(24, 'Listar Monedas', '/listadoMonedas', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(25, 'Modificar Monedas', '/modificarMonedas', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(26, 'Permisos', '/permisos', '2020-09-26 21:20:01', '2020-09-26 21:20:01');
/*!40000 ALTER TABLE `permisos` ENABLE KEYS */;

-- Dumping structure for table paydaybd.permisosderoles
CREATE TABLE IF NOT EXISTS `permisosderoles` (
  `IdRol` tinyint(3) unsigned NOT NULL,
  `IdPermiso` tinyint(3) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IdRol`,`IdPermiso`),
  KEY `FK_Permiso_PermisoUsuario` (`IdPermiso`),
  CONSTRAINT `FK_Permiso_PermisoUsuario` FOREIGN KEY (`IdPermiso`) REFERENCES `permisos` (`IdPermiso`),
  CONSTRAINT `FK_Rol_PermisoRol` FOREIGN KEY (`IdRol`) REFERENCES `roles` (`IdRol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.permisosderoles: ~0 rows (approximately)
/*!40000 ALTER TABLE `permisosderoles` DISABLE KEYS */;
/*!40000 ALTER TABLE `permisosderoles` ENABLE KEYS */;

-- Dumping structure for table paydaybd.personasempleados
CREATE TABLE IF NOT EXISTS `personasempleados` (
  `IdEmpleado` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdUsuario` int(10) unsigned NOT NULL,
  `Nombre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Apellido` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Documento` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Sexo` enum('Masculino','Femenino','Otro') COLLATE utf8mb4_unicode_ci NOT NULL,
  `FechaNacimiento` date NOT NULL,
  `IdRol` tinyint(3) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IdEmpleado`),
  UNIQUE KEY `personasempleados_idusuario_unique` (`IdUsuario`),
  UNIQUE KEY `personasempleados_documento_unique` (`Documento`),
  KEY `FK_Rol_PersonaEmpleado` (`IdRol`),
  CONSTRAINT `FK_Rol_PersonaEmpleado` FOREIGN KEY (`IdRol`) REFERENCES `roles` (`IdRol`),
  CONSTRAINT `FK_Usuario_PersonaEmpleado` FOREIGN KEY (`IdUsuario`) REFERENCES `usuarios` (`IdUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.personasempleados: ~0 rows (approximately)
/*!40000 ALTER TABLE `personasempleados` DISABLE KEYS */;
INSERT INTO `personasempleados` (`IdEmpleado`, `IdUsuario`, `Nombre`, `Apellido`, `Documento`, `Sexo`, `FechaNacimiento`, `IdRol`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Sistema', 'Informatico', '00000000', 'Otro', '2020-09-26', 4, '2020-09-26 21:20:01', '2020-09-26 21:20:01');
/*!40000 ALTER TABLE `personasempleados` ENABLE KEYS */;

-- Dumping structure for table paydaybd.personasfisicas
CREATE TABLE IF NOT EXISTS `personasfisicas` (
  `IdPersonaFisica` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdUsuario` int(10) unsigned NOT NULL,
  `Nombre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Apellido` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Documento` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Sexo` enum('Masculino','Femenino','Otro') COLLATE utf8mb4_unicode_ci NOT NULL,
  `FechaNacimiento` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IdPersonaFisica`),
  UNIQUE KEY `personasfisicas_idusuario_unique` (`IdUsuario`),
  UNIQUE KEY `personasfisicas_documento_unique` (`Documento`),
  CONSTRAINT `FK_Usuario_PersonaFisica` FOREIGN KEY (`IdUsuario`) REFERENCES `usuarios` (`IdUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.personasfisicas: ~2 rows (approximately)
/*!40000 ALTER TABLE `personasfisicas` DISABLE KEYS */;
INSERT INTO `personasfisicas` (`IdPersonaFisica`, `IdUsuario`, `Nombre`, `Apellido`, `Documento`, `Sexo`, `FechaNacimiento`, `created_at`, `updated_at`) VALUES
	(1, 2, 'Lorenzo', 'Ferreri', '51294519', 'Femenino', '1994-01-01', '2020-09-28 05:13:13', '2020-09-29 01:09:38'),
	(4, 10, 'Cri', 'Cri', '51394512', 'Masculino', '1994-01-01', '2020-09-28 06:31:26', '2020-09-28 06:31:26'),
	(5, 11, 'Testeador', 'PHP', '99999999', 'Otro', '1994-01-01', '2020-09-29 01:24:34', '2020-09-29 01:24:34');
/*!40000 ALTER TABLE `personasfisicas` ENABLE KEYS */;

-- Dumping structure for table paydaybd.personasjuridicas
CREATE TABLE IF NOT EXISTS `personasjuridicas` (
  `IdPersonaJuridica` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdUsuario` int(10) unsigned NOT NULL,
  `RazonSocial` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NombreFantasia` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Rut` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FormaJuridica` enum('UNI','SRL','SA','Otra') COLLATE utf8mb4_unicode_ci NOT NULL,
  `FechaConformacion` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IdPersonaJuridica`),
  UNIQUE KEY `personasjuridicas_idusuario_unique` (`IdUsuario`),
  UNIQUE KEY `personasjuridicas_rut_unique` (`Rut`),
  CONSTRAINT `FK_Usuario_PersonaJuridica` FOREIGN KEY (`IdUsuario`) REFERENCES `usuarios` (`IdUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.personasjuridicas: ~0 rows (approximately)
/*!40000 ALTER TABLE `personasjuridicas` DISABLE KEYS */;
/*!40000 ALTER TABLE `personasjuridicas` ENABLE KEYS */;

-- Dumping structure for table paydaybd.productoscatalogo
CREATE TABLE IF NOT EXISTS `productoscatalogo` (
  `IdProductoCatalogo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Descripcion` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CostoPuntos` mediumint(8) unsigned NOT NULL,
  `RutaImagen` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Activo` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IdProductoCatalogo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.productoscatalogo: ~0 rows (approximately)
/*!40000 ALTER TABLE `productoscatalogo` DISABLE KEYS */;
/*!40000 ALTER TABLE `productoscatalogo` ENABLE KEYS */;

-- Dumping structure for table paydaybd.publicidades
CREATE TABLE IF NOT EXISTS `publicidades` (
  `IdPublicidad` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `RutaImagen` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Activo` tinyint(1) NOT NULL,
  `FechaInicio` date NOT NULL,
  `FechaVencimiento` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IdPublicidad`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.publicidades: ~5 rows (approximately)
/*!40000 ALTER TABLE `publicidades` DISABLE KEYS */;
INSERT INTO `publicidades` (`IdPublicidad`, `Descripcion`, `RutaImagen`, `Activo`, `FechaInicio`, `FechaVencimiento`, `created_at`, `updated_at`) VALUES
	(1, 'prueba', 'img/publicidad/1.png', 0, '2020-09-26', '2020-10-03', '2020-09-26 21:37:44', '2020-09-27 02:37:46'),
	(2, 'otro mas', 'img/publicidad/2.png', 0, '2020-09-26', '2020-10-03', '2020-09-26 22:26:16', '2020-09-27 02:37:50'),
	(3, 'otra mas', 'img/publicidad/3.png', 0, '2020-09-26', '2020-10-03', '2020-09-26 22:26:25', '2020-09-27 02:37:54'),
	(4, 'Oca 20%', 'img/publicidad/4.png', 1, '2020-09-26', '2020-10-03', '2020-09-27 02:54:39', '2020-09-27 02:54:39'),
	(5, 'Santander loker', 'img/publicidad/5.png', 1, '2020-09-26', '2020-10-03', '2020-09-27 02:55:09', '2020-09-27 02:55:09');
/*!40000 ALTER TABLE `publicidades` ENABLE KEYS */;

-- Dumping structure for table paydaybd.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `IdRol` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Rol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Activo` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IdRol`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`IdRol`, `Rol`, `Activo`, `created_at`, `updated_at`) VALUES
	(1, 'Marketing', 1, '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(2, 'Operario', 1, '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(3, 'Moderador', 1, '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(4, 'Administrador', 1, '2020-09-26 21:20:01', '2020-09-26 21:20:01');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table paydaybd.servicios
CREATE TABLE IF NOT EXISTS `servicios` (
  `IdServicio` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Descripcion` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FechaRegistro` date NOT NULL,
  `Activo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IdServicio`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.servicios: ~0 rows (approximately)
/*!40000 ALTER TABLE `servicios` DISABLE KEYS */;
INSERT INTO `servicios` (`IdServicio`, `Nombre`, `Descripcion`, `FechaRegistro`, `Activo`, `created_at`, `updated_at`) VALUES
	(1, 'Oca', 'Oca card', '2020-09-28', 1, '2020-09-28 04:49:52', '2020-09-28 04:49:52'),
	(2, 'Santander', 'Santander', '2020-09-28', 1, '2020-09-28 04:50:09', '2020-09-28 04:50:09'),
	(3, 'UTE', 'Servicio de electricidad', '2020-09-28', 1, '2020-09-28 04:50:27', '2020-09-28 04:50:27');
/*!40000 ALTER TABLE `servicios` ENABLE KEYS */;

-- Dumping structure for table paydaybd.serviciosagendados
CREATE TABLE IF NOT EXISTS `serviciosagendados` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.serviciosagendados: ~0 rows (approximately)
/*!40000 ALTER TABLE `serviciosagendados` DISABLE KEYS */;
/*!40000 ALTER TABLE `serviciosagendados` ENABLE KEYS */;

-- Dumping structure for table paydaybd.tarjetas
CREATE TABLE IF NOT EXISTS `tarjetas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.tarjetas: ~0 rows (approximately)
/*!40000 ALTER TABLE `tarjetas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tarjetas` ENABLE KEYS */;

-- Dumping structure for table paydaybd.telefonosusuarios
CREATE TABLE IF NOT EXISTS `telefonosusuarios` (
  `IdTelefono` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdUsuario` int(10) unsigned NOT NULL,
  `Telefono` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Activo` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IdTelefono`),
  KEY `FK_Telefono_Usuario` (`IdUsuario`),
  CONSTRAINT `FK_Telefono_Usuario` FOREIGN KEY (`IdUsuario`) REFERENCES `usuarios` (`IdUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.telefonosusuarios: ~0 rows (approximately)
/*!40000 ALTER TABLE `telefonosusuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `telefonosusuarios` ENABLE KEYS */;

-- Dumping structure for table paydaybd.tiposusuario
CREATE TABLE IF NOT EXISTS `tiposusuario` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `TipoUsuario` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Activo` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.tiposusuario: ~0 rows (approximately)
/*!40000 ALTER TABLE `tiposusuario` DISABLE KEYS */;
INSERT INTO `tiposusuario` (`Id`, `TipoUsuario`, `Activo`, `created_at`, `updated_at`) VALUES
	(1, 'Persona Fisica', 1, '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(2, 'Persona Juridica', 1, '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(3, 'Empleado Payday', 1, '2020-09-26 21:20:01', '2020-09-26 21:20:01');
/*!40000 ALTER TABLE `tiposusuario` ENABLE KEYS */;

-- Dumping structure for table paydaybd.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `IdUsuario` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdTipoUsuario` tinyint(3) unsigned NOT NULL,
  `Email` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FechaRegistro` date NOT NULL,
  `TokenActivacion` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Puntos` int(10) unsigned NOT NULL DEFAULT '0',
  `Activo` tinyint(1) NOT NULL,
  `ApiToken` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IdUsuario`),
  UNIQUE KEY `usuarios_email_unique` (`Email`),
  UNIQUE KEY `usuarios_apitoken_unique` (`ApiToken`),
  KEY `FK_TipoUsuario_Usuario` (`IdTipoUsuario`),
  CONSTRAINT `FK_TipoUsuario_Usuario` FOREIGN KEY (`IdTipoUsuario`) REFERENCES `tiposusuario` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table paydaybd.usuarios: ~0 rows (approximately)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`IdUsuario`, `IdTipoUsuario`, `Email`, `Password`, `FechaRegistro`, `TokenActivacion`, `remember_token`, `Puntos`, `Activo`, `ApiToken`, `created_at`, `updated_at`) VALUES
	(1, 3, 'sistemas@netcodesolutions.com', '$2y$10$DacIXfaU1BesezMv30ishOSRONpEI/yFhLjanUV1H88edxpt2Qv6K', '2020-09-26', NULL, NULL, 0, 1, '213dXyNOTOKEN123$#23fdSA', '2020-09-26 21:20:01', '2020-09-26 21:20:01'),
	(2, 1, 'clferreri94@hotmail.com', '$2y$10$Z9nQm8BE1X1sTVEGFvN.3eUV8Q6ucD6MPiclIk0dKrZ5mbR57ZTiS', '2020-09-28', NULL, NULL, 2100, 1, NULL, '2020-09-28 05:12:56', '2020-09-29 01:12:29'),
	(3, 1, 'tony@hotma', '$2y$10$dQoK/s.oSUtZ48Lo3q8pj.r3hQzflrrkyRUXERs5twhHeoKKqpDJ6', '2020-09-28', NULL, NULL, 0, 1, NULL, '2020-09-28 05:31:28', '2020-09-28 05:31:28'),
	(5, 1, 's', '$2y$10$FblMLqau5c/U3nus.whAr.d9xhlyzm7pYqh9tH9mXBsgkkcCJWzdC', '2020-09-28', NULL, NULL, 0, 1, NULL, '2020-09-28 05:49:24', '2020-09-28 05:49:24'),
	(6, 1, 'hernan@hotmail.com', '$2y$10$oBVjo7XKEC1s9etk4f74j.cpFSZElQA/8Vcq.GfHjPq1wh7PompKi', '2020-09-28', NULL, NULL, 0, 1, NULL, '2020-09-28 05:53:21', '2020-09-28 05:53:21'),
	(7, 1, 'tuti@hotmail.com', '$2y$10$.TBgChQYwmjCnNv.3LBCK.eF0DemXDGW.vjG0cMJWbmJaYlvhgcNy', '2020-09-28', NULL, NULL, 0, 1, NULL, '2020-09-28 05:56:34', '2020-09-28 05:56:34'),
	(9, 1, 'tuti2@hotmail.com', '$2y$10$mxFUYnNPqig.h25giCGfmOUk/pXHkUCME./WsNRyiS5ycJ19LMdzq', '2020-09-28', NULL, NULL, 0, 1, NULL, '2020-09-28 05:57:38', '2020-09-28 05:57:38'),
	(10, 1, 'talia@hotmail.com', '$2y$10$DGZRvbjkdd.nYggbvw0pbe8RopP.HL1yV0XMERSvBrbdu6DvNkTEi', '2020-09-28', NULL, NULL, 0, 1, NULL, '2020-09-28 06:31:26', '2020-09-28 06:31:26'),
	(11, 1, 'tester@hotmail.com', '$2y$10$CPRkbF8o/ygGIax1ZCo9qO8vD7dJMy.ewvvBgevKIeREpS9zUtHja', '2020-09-29', NULL, NULL, 0, 1, NULL, '2020-09-29 01:24:34', '2020-09-29 01:24:34');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
