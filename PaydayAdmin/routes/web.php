<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Rutas Web
|--------------------------------------------------------------------------
|
| Encontraremos todas las rutas del sistema de administracion
| Todas se encuentran protegidas por el middleware que controla la sesion
|
*/

//Pagina de Iniciar Sesion
Route::get('/inicioSesion', 'LoginController@IniciarSesionView');

//Accion de iniciar sesion
Route::get('/iniciarSesion', 'LoginController@IniciarSesion')->name('IniciarSesionAccion');



Route::middleware('VerificarSesion')->group(function () {

    Route::get('/CargarPermisos', 'AjaxController\PermisosController@ListarPermisos');

        // PETICIONES POR AJAX //////////////////////
        Route::post('/AltaEmpleadoAjax', 'AjaxController\UsuarioController@AltaEmpleado');

        Route::post('/ABMServiciosAjax', 'AjaxController\ServicioController@ABMServicio');
        Route::post('/ABMMetodosDePago', 'AjaxController\MetodosDePagoController@ABMMetodoPago');
        Route::post('/ActualizarPermisos', 'AjaxController\PermisosController@ActualizarPermisos');
        ////////////////////////////////////////////
        Route::get('/', 'ModulosController@Dashboard')->name('inicio');
        Route::get('/cerrarSesion', 'LoginController@CerrarSesion')->name('CerrarSesion');
        Route::get('/dashboard', 'ModulosController@Dashboard')->name('Dashboard');

        Route::get('/agregarEmpleado','ModulosController@AltaEmpleado')->name('AltaEmpleado');
        Route::get('/modificarEmpleado/{idUsuario}', 'ModulosController@ModificarEmpleado')->name('ModificarEmpleado');
        Route::get('/listadoEmpleados', 'ModulosController@ListadoEmpleados')->name('ListadoEmpleados');
        Route::get('/listadoClientes', 'ModulosController@ListadoClientes')->name('ListadoClientes');

        Route::get('/listadoServicios', 'ModulosController@ListadoServicios')->name('ListadoServicios');

        Route::get('/listadoMetodos', 'ModulosController@ListadoMetodosDePago')->name('ListadoMetodo');

        Route::post('/ModificarEmpleadoAjax', 'AjaxController\UsuarioController@ModificarEmpleado');
        Route::get('/permisos', 'ModulosController@ListadoPermisos')->name('ListadoPermisos');
        Route::get('/listadoMonedas', 'ModulosController@ListadoMonedas')->name('ListadoMonedas');



    //PUBLICIDAD//////
    Route::get('/listadoPublicidad', 'ModulosController@ListadoPublicidad')->name('ListadoPublicidad');

    Route::post('/altaPublicidad', 'AjaxController\PublicidadController@AltaPublicidad')->name("altaPublicidad");
    Route::post('/bajaPublicidad', 'AjaxController\PublicidadController@BajaPublicidad')->name("bajaPublicidad");
    Route::put('/modificarPublicidad', 'AjaxController\PublicidadController@ModificarPublicidad')->name('modificarPublicidad');

    //PRODUCTO//////
    Route::get('/agregarProducto', 'ModulosController@AltaProducto')->name("AgregarProducto");
    Route::get('/modificarProducto/{idProducto}', 'ModulosController@ModificarProducto')->name("ModificarProducto");
    Route::get('/listadoProductos', 'ModulosController@ListadoProductos')->name('ListarProductos');
    Route::post('/altaProducto', 'AjaxController\ProductosController@AltaProducto')->name("AltaProducto");

    //PAGOS///
    Route::get('/pagos', 'ModulosController@ListarPagos')->name('ListadoPagos');
    Route::post('/bajaPago', 'AjaxController\PagosController@BajaPago')->name('BajaPago');
});

    Route::view('/noPermiso403', 'Errores.NoPermisos');



