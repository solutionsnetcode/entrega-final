
$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#fupForm").on('submit', function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'altaPublicidad',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                RenderizarTable("publicidadTable", data);
                $("#mdlNuevaPublicidad").modal("hide");
            }
        });
    });

    $("img").click(function(){
        var imagen = $(this);
        PrevisualizarImagen(imagen.attr('src'));
    });

});



function AbrirModal(){
    var fecha = new Date();
    var fechaActual = fecha.getFullYear() + "-" + ( (fecha.getMonth() < 9) ? "0" + (fecha.getMonth() + 1) : (fecha.getMonth() + 1)) + "-" + ((fecha.getDate() < 10) ? "0" + fecha.getDate() : fecha.getDate());
    fecha.setDate(fecha.getDate() + 7);
    var fechaFinal = fecha.getFullYear() + "-" + ( (fecha.getMonth() < 9) ? "0" + (fecha.getMonth() + 1) : (fecha.getMonth() + 1)) + "-" + ((fecha.getDate() < 10) ? "0" + fecha.getDate() : fecha.getDate());
    $("#fileImagen").val("");
    $("#txtDescripcion").val("");
    $("#dtpInicio").val(fechaActual);
    $("#dtpFin").val(fechaFinal);
    $("#mdlNuevaPublicidad").modal("show");
}


function AltaPublicidad(){
    var data = new FormData($("#formPublicidad")[0]);

    if ($("#fileImagen").val() == "" || $("#txtDescripcion").val().trim() == ""){
        alertaToast(tipoAlertaError, "El archivo y la descripcion son obligatorios", 3500);
    }else{
        $("#mantaLoading").modal('show');
        $.ajax({
            url:'altaPublicidad',
            data:{'prueba': data},
            processData: false,
            contentType: false,
            type:'post',
            dataType: "json",
            success: function (response) {
                console.log(response);
                var tabla = $('#metodosTable').DataTable();
                tabla.clear();
                $.each( response, function( key, value ) {
                tabla.row.add(value);
              });
              tabla.draw();
              $("·")
                alertaToast(tipoAlertaOK, "Operacion realizada.", 3500);
                $("#mantaLoading").modal('hide');
        },
        statusCode: {
            404: function() {
            $("#mantaLoading").modal('hide');
            alertaToast(tipoAlertaError, 'El servicio Payday no se encuentra disponible', 3500);
            }
        },
        error:function(x,xs,xt){
            $("#mantaLoading").modal('hide');
            console.log(x);
            console.log(xs);
            }
        });
    }
}

function EditarFecha(){
    var idPublicidad = $("#txtIdPublicidadEditar").val();
    var fechaInicio = $("#dtpFechaInicioEditar").val();
    var fechaFin = $("#dtpFechaFinEditar").val();

    var fechaInicioComparar = new Date(fechaInicio);
    var fechaFinComparar = new Date(fechaFin);
    var fechaActualComparar = new Date();

    if (fechaInicioComparar > fechaFinComparar){
        $("#mdlEditarFecha").modal("hide");
        Swal.fire({
            icon: 'error',
            title: 'No se puede actualizar la fecha',
            position: 'center',
            html: 'La fecha inicial no puede ser menor que la final',
            confirmButtonText: "Aceptar",
            showClass: {
                popup: 'animated rubberBand'
              },
            hideClass: {
                popup: 'animated fadeOut faster'
              }
          })
    } else if(fechaFinComparar < fechaActualComparar){
        $("#mdlEditarFecha").modal("hide");
        Swal.fire({
            icon: 'error',
            title: 'No se puede actualizar la fecha',
            position: 'center',
            html: 'La fecha final no puede ser menor a la actual',
            confirmButtonText: "Aceptar",
            showClass: {
                popup: 'animated rubberBand'
              },
            hideClass: {
                popup: 'animated fadeOut faster'
              }
          })
    }

    else{
        $.ajax({
            type: 'PUT',
            url: 'modificarPublicidad',
            data: {'idPublicidad' : idPublicidad,  'fechaInicio' : fechaInicio, 'fechaFin' : fechaFin},
            dataType: "json",
            success: function(data){
                RenderizarTable("publicidadTable", data);
                $("#mdlEditarFecha").modal("hide");
            }
        });
    }
}



function AbrirEditarFecha(id, fechaInicio, fechaFin){
    $("#txtIdPublicidadEditar").val(id);
    $("#dtpFechaInicioEditar").val(fechaInicio);
    $("#dtpFechaFinEditar").val(fechaFin);
    $("#mdlEditarFecha").modal('show');
}

function ConfirmarEditarFecha(){
    var fechaInicio = $("#dtpFechaInicioEditar").val();
    var fechaFin =  $("#dtpFechaFinEditar").val();
    $("#mdlEditarFecha").modal('hide');
    EditarFecha(fechaInicio, fechaFin);
}

function DesactivarPublicidad(idPublicidad){
    Swal.fire({
        icon: 'error',
        title: '¿Desea anular la publicidad?',
        position: 'center',
        html: 'Esta operación es irreversible.',
        confirmButtonText: "Desactivar",
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        cancelButtonColor: '#d33',

        showClass: {
            popup: 'animated rubberBand'
          },
        hideClass: {
            popup: 'animated fadeOut faster'
          }
      }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: 'POST',
                url: 'bajaPublicidad',
                dataType: "json",
                data: {'publicidad' : idPublicidad},
                success: function(data){
                    Swal.fire(
                        'Anulacion',
                        'Se anulo correctamente.',
                        'success'
                      )
                    RenderizarTable("publicidadTable", data);
                }
            });

        }
      })
}

function PrevisualizarImagen(url){
    $("#imgMuestra").attr("src",url);
    $("#mdlImagen").modal("show");
}

function LimpiarModal(){
    $("#txtIdMetodoPago").val("");
    $("#txtMetodoDePago").val("");
    $("#txtDescripcion").val("");
    $("#txtUrlPago").val("");
    $("#mdlNuevoServicio").modal("hide");
}






