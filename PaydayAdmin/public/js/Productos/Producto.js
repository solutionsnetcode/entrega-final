$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#formularioNuevoProducto").on('submit', function(e){
        e.preventDefault();
        if (validarDatos($("#txtNombre").val(), $("#txtDescripcion").val(), $("#txtPuntos").val(), $("#ifImagenProducto").val())){
            $.ajax({
                type: 'POST',
                url: 'altaProducto',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                success: function(data){
                    alertaToast('success', 'Nuevo producto agregado', 3000);
                    $("#formularioNuevoProducto").trigger("reset");
                },
                error: function(data){
                    alertaToast('error', data['responseJSON']['message'], 3000);
                },
                statusCode: {
                    404: function() {
                        alertaToast('error', 'Error al contectarse con el servicio de payday.');
                    }
                }
            });
        }else{
            Swal.fire({
                icon: 'error',
                title: 'No se puede dar de alta el producto',
                position: 'center',
                html: 'Verifique que ingreso todos los datos',
                confirmButtonText: "Aceptar",
                showClass: {
                    popup: 'animated rubberBand'
                  },
                hideClass: {
                    popup: 'animated fadeOut faster'
                  }
              })
        }

    });

    $("img").click(function(){
        var imagen = $(this);
        PrevisualizarImagen(imagen.attr('src'));
    });
});





function validarDatos(nombre, descripcion, puntos, imagen){
    if (nombre.trim() == "" || descripcion.trim() == "" || isNaN(puntos) || puntos <= 0 || imagen.trim() == ""){
        return false;
    }
    return true;
}

function PrevisualizarImagen(url){
    $("#imgMuestra").attr("src",url);
    $("#mdlImagen").modal("show");
}

