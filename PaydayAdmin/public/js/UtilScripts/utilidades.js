function ComaPorPunto(id){
    var valor = $("#"+id).val();
    var valor = valor.replace(",",".");
    $("#" + id).val(valor);
}

/**
 * Limpia el Datatable y lo renderiza con la informacion que se le pasa como array
 * @author Cristian Ferreri
 *
 * @param contenido Array con todos los datos (debe tener la misma cantidad de valores que columnas)
 *
 */
function RenderizarTable(idDataTable, contenido){
    console.log(contenido);
    var tabla = $('#' + idDataTable).DataTable();
    tabla.clear();
    $.each(contenido, function( key, value ) {
        tabla.row.add(Object.values(value));
    });
    tabla.draw();
}
