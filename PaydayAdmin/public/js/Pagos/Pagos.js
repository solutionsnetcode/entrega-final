$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

});// FIN READY




function AnularPago(idPago){
    Swal.fire({
        icon: 'error',
        title: '¿Desea anular el pago?',
        position: 'center',
        html: 'Esta operación es irreversible.',
        confirmButtonText: "Anular",
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        cancelButtonColor: '#d33',

        showClass: {
            popup: 'animated rubberBand'
          },
        hideClass: {
            popup: 'animated fadeOut faster'
          }
      }).then((result) => {
        if (result.isConfirmed) {
            alert('aqui');
            $.ajax({
                type: 'POST',
                url: 'bajaPago',
                dataType: "json",
                data: {'pago' : idPago},
                success: function(data){
                    Swal.fire(
                        'Anulacion',
                        'Se anulo correctamente.',
                        'success'
                      )
                    RenderizarTable("publicidadTable", data);
                }
            });

        }
      })
}
