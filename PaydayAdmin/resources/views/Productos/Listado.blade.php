@extends('layouts/layout')

@section('menu-producto-catalogo')
    active
@endsection

@section('link-producto-listar')
    active
@endsection

@section('styles')

  <style>
    .badge:hover{
      cursor: pointer;
    }

    .link{
      cursor:pointer;
    }


  </style>

	<link href="{{asset("assets/$AdminPanel/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css") }}" rel="stylesheet" />
	<link href="{{asset("assets/$AdminPanel/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css") }}" rel="stylesheet" />
	<link href="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css") }}" rel="stylesheet" />
  <link href="{{ asset("assets/$AdminPanel/plugins/switchery/switchery.min.css") }}" rel="stylesheet" />
@endsection


@section('contenido')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1>Listado de Productos</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Productos</a></li>
            </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>



  <div class="row">
    <div class="panel panel-primary w-100">
      <div class="panel-heading">
        <h4 class="panel-title">Listado</h4>
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-reload" onclick="RecargarClientes();"><i class="fa fa-redo"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
        </div>
      </div>
      <br>
      <button class="ml-3 btn btn-success">Nuevo Producto +</button>
      <div class="panel-body table-responsive">
        <table id="productoTable" class="table table-bordered table-hover">
          <thead>
            <tr class="text-center">
                <th>Imagen</th>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Costo</th>
                <th>Activo</th>
                <th>Modificar</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($productos as $producto)
            <tr>
                <td>
                    <img src="{{$producto->RutaImagen}}" width="180px" alt="{{$producto->RutaImagen}}" />
                </td>
                <td>
                    {{$producto->Nombre}}
                </td>
                <td>
                    {{$producto->Descripcion}}
                </td>
                <td>
                    {{$producto->CostoPuntos}}
                </td>
                <td>
                    @if ($producto->Activo)
                        <span class="text-green">Activo</span>
                    @else
                        <span class="text-danger">Inactivo</span>
                    @endif
                </td>
                <td>
                    <a class="text-secondary" href="{{ route('ModificarProducto', ['idProducto' => $producto->IdProductoCatalogo]) }}"><i class="fas fa-pencil-alt fa-2x"></i></a>
                </td>
              </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>

        <!-- MODAL -->
        <div class="modal inmodal" style="z-index: 1100;" id="mdlImagen" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated bounceInRight">
                    <div class="modal-header" style="margin-top: -20px;">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                        <br/>
                        <h4 id="tituloModalEditar" class="modal-title text-center">Visualizar Imagen</h4>
                    </div>
                        <div class="modal-body">
                            <img id="imgMuestra" src="" alt="prueba" width="450px">
                        </div>
                </div>
            </div>
        </div>
  </div>



@endsection

@section('scripts')

<script src="{{asset("assets/$AdminPanel/plugins/datatables.net/js/jquery.dataTables.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-responsive/js/dataTables.responsive.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons/js/dataTables.buttons.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons/js/buttons.colVis.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons/js/buttons.flash.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons/js/buttons.html5.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons/js/buttons.print.min.js") }}"></script>
<script src="{{asset("js/Productos/Producto.js")}}"></script>
<script src="{{ asset("assets/$AdminPanel/plugins/switchery/switchery.min.js") }}"></script>
<script src="{{ asset("assets/$AdminPanel/js/demo/form-slider-switcher.demo.js") }}"></script>

<script>



    $(document).ready(function() {

      $('#productoTable').DataTable({
        pageLength: 25,
        lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'Todos']],
      //para cambiar el lenguaje a español

          "language": {

                  "lengthMenu": "Mostrar _MENU_ registros",
                  "zeroRecords": "No se encontraron resultados",
                  "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                  "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                  "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                  "sSearch": "Buscar:",
                  "oPaginate": {
                      "sFirst": "Primero",
                      "sLast":"Último",
                      "sNext":"Siguiente",
                      "sPrevious": "Anterior"
                   },
                   "sProcessing":"Procesando...",

              },
              "columnDefs": [
                { className: "text-center align-middle", "targets": [0,1,2,3,4,5] },
              ]

      });

  });



  </script>

@endsection
