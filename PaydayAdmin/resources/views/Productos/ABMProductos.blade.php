@extends('layouts/layout')

@section('menu-producto-catalogo')
    active
@endsection

@section('link-producto-generar')
    active
@endsection

@section('styles')
    <link href="{{ asset("assets/$AdminPanel/plugins/switchery/switchery.min.css") }}" rel="stylesheet" />
@endsection

@section('contenido')

 <!-- CUERPO DE LA PAGINA-->

@isset($producto)

  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Editar Producto</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Producto</a></li>
            <li class="breadcrumb-item"><a href="{{route('ListadoEmpleados')}}">Listado Productos</a></li>
            <li class="breadcrumb-item active">Editar Producto</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

@else

  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Generar Producto</h1>
        </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Inicio</a></li>
          <li class="breadcrumb-item active">Generar Producto</li>
        </ol>
      </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

@endisset

@isset($producto)

  <!-- Main content -->
  <div class="row">
    <div class="col-12 p-0">
      <div class="card card-primary">
        <div class="card-header pb-0">
          <a class="float-right align-middle mb-1" href="{{route('ListadoEmpleados')}}"><i class="fas fa-arrow-alt-circle-left fa-lg"></i> Ir al listado</a>
          <input type="text" value="{{$producto->IdProductoCatalogo}}"  id="idUsuario" hidden>
        </div>
        <form role="form" id="formularioNuevoProducto" enctype="multipart/form-data">
            <div class="card-body">
            <input type="text" value="{{$producto->IdProductoCatalogo}}" id="txtProductoCatalogo" name="productoCatalogo" hidden/>
                <div class="row">
                <div class="form-group col-12 col-md-6">
                  <label for="txtNombre">Nombre</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text d-none d-sm-block"><i class="fas fa-envelope"></i></span>
                    </div>
                        <input required type="text" class="form-control" id="txtNombre" value="{{$producto->Nombre}}" name="nombre">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-12">
                  <label for="exampleInputPassword1">Descripción</label>
                    <textarea class="form-control col-12" name="descripcion" id="txtDescripcion" cols="30" rows="2" required>{{$producto->Descripcion}}</textarea>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-12 col-md-6">
                  <label for="exampleInputPassword1">Costo de Puntos</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text d-none d-sm-block"><i class="fas fa-address-card"></i></span>
                      </div>
                    <input type="number" class="form-control" id="txtPuntos" value="{{$producto->CostoPuntos}}" name="puntos" min="1">
                    </div>
                    <br/>
                    <br/>
                    <label for="image">Activo</label>
                    @if ($producto->Activo)
                    <input class="chk ml-4" id="chk" type="checkbox" data-render="switchery" data-theme="blue" checked/>
                    @else
                    <input class="chk ml-4" id="chk" type="checkbox" data-render="switchery" data-theme="blue"/>
                    @endif
                </div>
                <div class="form-group col-12 col-md-6">
                  <label for="cmbSexo">Imagen</label>
                  <br/>
                  <input class="form-control" type="file" name="fileImagenName" id="ifImagenProducto" required>
                    <br/>
                  <img src="../{{$producto->RutaImagen}}" alt="error al cargar imagen" width="260px;"/>

                </div>
              </div>
              <div class="row">
                <div class="form-group col-12 col-md-6">

                  </div>
              </div>
            </div>
          </form>
        <!-- form start -->
        <form role="form" id="formularioNuevoCliente">
          <div class="card-body">
          </div><!-- /.card-body -->
          <div id="pnlPieBotones" class="card-footer text-right">
            <a href="{{route('ListadoEmpleados')}}" class="btn btn-success m-auto col-12 col-md-6 col-xl-3">Volver</a>
            <button type="button" id="btnActualizarEmpleado" class="btn btn-green m-auto col-12 col-md-6 col-xl-3">Actualizar</button>
          </div>
        </form>
      </div><!-- /.card -->
    </div>
  </div>

@else

  <div class="row">
    <div class="col-12">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title mb-0">Datos del Producto</h3>
        </div>
          <!-- form start -->
        <form role="form" id="formularioNuevoProducto" enctype="multipart/form-data">
          <div class="card-body">
            <div class="row">
              <div class="form-group col-12 col-md-6">
                <label for="txtNombre">Nombre</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text d-none d-sm-block"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input required type="text" class="form-control" id="txtNombre" placeholder="Ingresa un nombre" name="nombre">
                </div>
              </div>

            </div>
            <div class="row">
              <div class="form-group col-12">
                <label for="exampleInputPassword1">Descripción</label>
                <textarea class="form-control col-12" name="descripcion" id="txtDescripcion" cols="30" rows="2" required></textarea>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-12 col-md-6">
                <label for="exampleInputPassword1">Costo de Puntos</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text d-none d-sm-block"><i class="fas fa-address-card"></i></span>
                    </div>
                    <input type="number" class="form-control" id="txtPuntos" placeholder="Valor puntos" name="puntos" min="1">
                  </div>
              </div>
              <div class="form-group col-12 col-md-6">
                <label for="cmbSexo">Imagen</label>
                <div class="input-group">
                  <input class="form-control" type="file" name="fileImagenName" id="ifImagenProducto" required>
                </div>
              </div>
            </div>
          </div>

          <div class="card-footer text-right">
            <button type="submit" id="btnAgregarProducto" class="btn btn-green m-auto col-12 col-md-6 col-xl-3">Agregar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endisset


@endsection





  @section('scripts')
    <script src="{{ asset('js/Productos/Producto.js') }}"></script>
    @isset($producto)
      <script src="{{ asset("assets/$AdminPanel/plugins/switchery/switchery.min.js") }}"></script>
      <script src="{{ asset("assets/$AdminPanel/js/demo/form-slider-switcher.demo.js") }}"></script>
      <script src="{{ asset('js/Usuarios/ModificarUsuario.js') }}"></script>
    @else
      <script src="{{ asset('js/Usuarios/AltaUsuario.js') }}"></script>
    @endisset

    <script src="{{ asset('js/UtilScripts/validacionesGenerales.js') }}"></script>


  @endsection
