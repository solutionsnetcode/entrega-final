@extends('layouts/layout')

@section('menu-dashboard')
    active
@endsection


@section('contenido')

    <!-- Migas de pan -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="javascript:;">Panel de Administración</a></li>
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
    <!-- /Migas de pan-->

    @csrf
    <!-- Titulo Pagina -->
    <h1 class="page-header">Dashboard</h1>
    <!-- /Titulo Pagina -->

    <div class="row">
        <div class="col-12 col-sm-6 col-xl-3">
            <!-- begin widget-stats -->
                <div class="widget widget-stats bg-gradient-green m-b-10">
                    <div class="stats-icon stats-icon-lg"><i class="fas fa-paper-plane"></i></div>
                    <div class="stats-content">
                        <div class="stats-title">PAGOS REALIZADOS</div>
                        <div data-animation="number" data-value="{{$datos['totalPagos']}}" class="stats-number">0</div>
                    </div>
                </div>
      <!-- end widget-stats -->
        </div>

        <div class="col-12 col-sm-6 col-xl-3">
            <!-- begin widget-stats -->
                <div class="widget widget-stats bg-gradient-lime m-b-10">
                    <div class="stats-icon stats-icon-lg"><i class="fas fa-dollar-sign"></i></div>
                    <div class="stats-content">
                        <div class="stats-title">Moneda Popular</div>
                        <div class="stats-number">{{$datos['monedaPopular']}}</div>
                    </div>
                </div>
                </div>
      <!-- end widget-stats -->

        <div class="col-12 col-sm-6 col-xl-3">
            <!-- begin widget-stats -->
                <div class="widget widget-stats bg-gradient-danger m-b-10">
                    <div class="stats-icon stats-icon-lg"><i class="fas fa-ban"></i></div>
                    <div class="stats-content">
                        <div class="stats-title">ANULACIONES</div>
                    <div class="stats-number" data-animation="number" data-value="{{$datos['pagosAnulados']}}">0</div>
                    </div>
                </div>
                </div>
      <!-- end widget-stats -->

        <div class="col-12 col-sm-6 col-xl-3">
            <!-- begin widget-stats -->
            <div class="widget widget-stats bg-gradient-warning m-b-10">
                <div class="stats-icon stats-icon-lg"><i class="fas fa-user-friends"></i></div>
                <div class="stats-content">
                    <div class="stats-title">NUEVOS USUARIOS</div>
                    <div class="stats-number">{{$datos['nuevosUsuarios']}}</div>
                </div>
            </div>
        </div>
      <!-- end widget-stats -->
    </div>
    <div class="row">
        <div class="col-6">
            <div class="panel panel-primary w-100">
                <div class="panel-heading">
                    <h4 class="panel-title">Servicios mas pagados</h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    </div>
                </div>
                <div class="panel-body table-responsive">
                    <table id="publicidadTable" class="table table-bordered table-hover text-center">
                        <thead>
                            <tr class="text-center">
                                <th class="w-25">Cant. Pagos</th>
                                <th class="w-75">Servicio</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($datos['ServiciosPupulares'] as $servicio)
                                <tr>
                                    <td>{{$servicio['total']}}</td>
                                    <td>{{$servicio['Servicio']}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="panel panel-primary w-100">
                <div class="panel-heading">
                    <h4 class="panel-title">Productos mas canjeados</h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    </div>
                </div>
                <div class="panel-body table-responsive">
                    <table id="publicidadTable" class="table table-bordered table-hover text-center">
                        <thead>
                            <tr class="text-center">
                                <th class="w-25">Cant. Canjes</th>
                                <th class="w-75">Producto</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($datos['ProductosPopulares'] as $producto)
                                <tr>
                                    <td>{{$producto['total']}}</td>
                                    <td>{{$producto['Nombre']}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
