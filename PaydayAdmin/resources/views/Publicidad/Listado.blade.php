@extends('layouts/layout')

@section('menu-publicidad')
    active
@endsection

@section('styles')

  <style>
    .badge:hover{
      cursor: pointer;
    }

    .link{
      cursor:pointer;
    }
  </style>

	<link href="{{asset("assets/$AdminPanel/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css") }}" rel="stylesheet" />
	<link href="{{asset("assets/$AdminPanel/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css") }}" rel="stylesheet" />
	<link href="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css") }}" rel="stylesheet" />
  <link href="{{ asset("assets/$AdminPanel/plugins/switchery/switchery.min.css") }}" rel="stylesheet" />
@endsection


@section('contenido')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1>Listado de Publicidad</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Publicidades</a></li>
            </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>



  <div class="row">
    <div class="panel panel-primary w-100">
      <div class="panel-heading">
        <h4 class="panel-title">Listado</h4>
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-reload" onclick="RecargarClientes();"><i class="fa fa-redo"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
        </div>
      </div>
      <br>
      <button onclick="AbrirModal();" class="ml-3 btn btn-success">Nueva Publicidad +</button>
      <div class="panel-body table-responsive">
        <table id="publicidadTable" class="table table-bordered table-hover">
          <thead>
            <tr class="text-center">
                <th>Imagen</th>
                <th>Descripción</th>
                <th>Duracion</th>
                <th>Activo</th>
                <th>Modificar / Baja</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($publicidades as $publicidad)
            <tr>
                <td>
                    {!! $publicidad['RutaImagen'] !!}
                </td>
                <td>
                    {{$publicidad['Descripcion']}}
                </td>
                <td>
                    {{$publicidad['Duracion']}}
                </td>
                <td>
                    @if ($publicidad['Activo'] == 1)
                        <span class="text-green">Activo</span>
                    @else
                        <span class="text-red">Inactivo</span>
                    @endif
                </td>
            <td>
                {!! $publicidad['Botones'] !!}
            </td>
              </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
       <!-- MODAL -->
   <div class="modal inmodal" style="z-index: 1100;" id="mdlNuevaPublicidad" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <form name="fupForm" enctype="multipart/form-data" id="fupForm" method="POST">
                <div class="modal-header" style="margin-top: -20px;">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                    <br/>
                    <h4 id="tituloModalEditar" class="modal-title text-center">Nueva Publicidad</h4>
                    <small class="font-bold col-12">No olvide guardar para aplicar los cambios.</small>
                </div>
                <div class="modal-body">
                    <div class="row mt-2">
                        <div class="col-12 mb-4">
                            <label for="fileImagen" id="lblImagen">Imagen</label>
                            <input type="file" class="form-control" value="" id="fileImagen" name="fileImagenName" required>
                        </div>

                        <div class="col-12 mb-4">
                            <label for="txtDescripcion" id="lblNombre">Descripcion</label>
                            <textarea id="txtDescripcion" name="descripcion" cols="30" class="form-control" rows="4"></textarea>
                        </div>

                        <div class="col-6 mb-4">
                            <label for="dtpInicio" id="lblFechaInicio">Fecha Inicio</label>
                            <input type="date" class="form-control" value="" id="dtpInicio" name="fechaInicio" required>
                        </div>

                        <div class="col-6 mb-4">
                            <label for="dtpFin" id="lblNombre">Fecha Vencimiento</label>
                            <input type="date" class="form-control" value="" id="dtpFin" name="fechaFin" required>
                        </div>

              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button id="btnAltaPublicidad" type="submit" class="btn btn-green">Cargar Publicidad</button>
            </div>
            </form>
        </div>
    </div>
</div>

     <!-- MODAL -->
     <div class="modal inmodal" style="z-index: 1100;" id="mdlEditarFecha" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header" style="margin-top: -20px;">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                    <br/>
                    <h4 id="tituloModalEditar" class="modal-title text-center">Editar Fecha Publicidad</h4>
                    <small class="font-bold col-12">No olvide guardar para aplicar los cambios.</small>
                </div>
                    <div class="modal-body">
                        <div class="row mt-2">
                            <input type="text" id="txtIdPublicidadEditar" hidden>
                            <div class="col-6 mb-1">
                                <label for="dtpFechaInicioEditar">Fecha Inicio</label>
                                <input type="date" class="form-control" value="" id="dtpFechaInicioEditar" name="fechaInicio" required>
                            </div>

                            <div class="col-6 mb-1">
                                <label for="dtpFechaFinEditar">Fecha Vencimiento</label>
                                <input type="date" class="form-control" value="" id="dtpFechaFinEditar" name="fechaFin" required>
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <button id="btnAltaPublicidad" onclick="EditarFecha();" type="button" class="btn btn-green">Actualizar Fechas</button>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL -->
    <div class="modal inmodal" style="z-index: 1100;" id="mdlImagen" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header" style="margin-top: -20px;">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                    <br/>
                    <h4 id="tituloModalEditar" class="modal-title text-center">Visualizar Imagen</h4>
                </div>
                    <div class="modal-body">
                        <img id="imgMuestra" src="" alt="prueba" width="450px">
                    </div>
            </div>
        </div>
    </div>
  </div>



@endsection

@section('scripts')

<script src="{{asset("assets/$AdminPanel/plugins/datatables.net/js/jquery.dataTables.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-responsive/js/dataTables.responsive.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons/js/dataTables.buttons.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons/js/buttons.colVis.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons/js/buttons.flash.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons/js/buttons.html5.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons/js/buttons.print.min.js") }}"></script>
<script src="{{asset("js/Publicidad/Publicidad.js")}}"></script>
<script src="{{ asset("assets/$AdminPanel/plugins/switchery/switchery.min.js") }}"></script>
<script src="{{ asset("assets/$AdminPanel/js/demo/form-slider-switcher.demo.js") }}"></script>

<script>



    $(document).ready(function() {

      $('#publicidadTable').DataTable({
        pageLength: 25,
        lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'Todos']],
      //para cambiar el lenguaje a español

          "language": {

                  "lengthMenu": "Mostrar _MENU_ registros",
                  "zeroRecords": "No se encontraron resultados",
                  "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                  "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                  "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                  "sSearch": "Buscar:",
                  "oPaginate": {
                      "sFirst": "Primero",
                      "sLast":"Último",
                      "sNext":"Siguiente",
                      "sPrevious": "Anterior"
                   },
                   "sProcessing":"Procesando...",

              },
              "columnDefs": [
                { className: "text-center align-middle", "targets": [0,1,2,3,4] },
              ]

      });

  });



  </script>

@endsection
