<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class PermisoRol extends Model
{
    protected $table = 'PermisosDeRoles';
    protected $primaryKey = ['IdRol', 'IdPermiso' ];

    protected $fillable = [
        'IdRol', 'IdPermiso'
    ];
}
