<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    protected $table = 'Permisos';
    protected $primaryKey = 'IdPermiso';

    protected $fillable = [
        'Permiso', 'Uri'
    ];
}
