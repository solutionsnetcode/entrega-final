<?php

namespace App\Modelos;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $table = 'pagos';
    protected $primaryKey = 'IdPago';

    protected $fillable = [
        'IdUsuario', 'IdMoneda', 'MontoPago', 'PuntosGenerados', 'IdServicio', 'FechaGenera', 'FechaPaga', 'IdMedioPago', 'Anulado'
    ];



    public function Usuario(){
        return $this->hasOne(User::class, 'IdUsuario' ,'IdUsuario');
    }

    public function Servicio(){
        return $this->hasOne(Servicio::class, 'IdServicio' ,'IdServicio');
    }

    public function Moneda(){
        return $this->hasOne(Moneda::class, 'IdMoneda' ,'IdMoneda');
    }

    public function MedioPago(){
        return $this->hasOne(MedioDePago::class, 'IdMedioPago' ,'IdMedioPago');
    }
}
