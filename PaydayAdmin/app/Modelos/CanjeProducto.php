<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class CanjeProducto extends Model
{
    protected $table = 'CanjeUsuariosProductos';
    protected $primaryKey = 'IdCanje';

    protected $fillable = [
        'IdUsuario', 'IdProductoCatalogo', 'FechaRealizado', 'Anulado'
    ];
}
