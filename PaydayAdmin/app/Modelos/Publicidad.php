<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Publicidad extends Model
{
    protected $table = 'Publicidades';
    protected $primaryKey = 'IdPublicidad';

    protected $fillable = [
        'Descripcion', 'RutaImagen', 'Activo', 'FechaInicio', 'FechaVencimiento'
    ];
}
