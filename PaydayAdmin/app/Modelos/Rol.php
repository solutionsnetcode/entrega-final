<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Rol extends Model
{
    protected $table = 'Roles';
    protected $primaryKey = 'IdRol';

    protected $fillable = [
        'Rol', 'Activo'
    ];

    public function ListarPermisos(){
        if ($this->Rol == "Administrador"){
            return DB::table('Permisos')->pluck('Permiso');
        }
        return DB::table('Permisos')->join('PermisosDeRoles', 'Permisos.IdPermiso', '=', 'PermisosDeRoles.IdPermiso')->where('PermisosDeRoles.IdRol', $this->IdRol)->pluck('Permiso');
    }

    public function Id(){
        return $this->IdRol;
    }
}
