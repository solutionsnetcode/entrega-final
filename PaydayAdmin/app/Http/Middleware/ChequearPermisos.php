<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class ChequearPermisos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $permiso = DB::table('Permisos')
        ->join('PermisosDeRoles', 'Permisos.IdPermiso', '=', 'PermisosDeRoles.IdPermiso')
        ->where('Permisos.Uri', '/' . $request->path())
        ->where('PermisosDeRoles.IdRol', $request->user()->DatosUsuario->Rol->IdRol)
        ->first();
        if($permiso != null){
            return $next($request);
        }else{
            return redirect('/noPermiso403');
        }

    }
}
