<?php

namespace App\Http\Controllers;

use App\Modelos\CanjeProducto;
use App\Modelos\MedioDePago;
use App\Modelos\Moneda;
use App\Modelos\Pago;
use App\Modelos\Permiso;
use App\Modelos\ProductoCatalogo;
use App\Modelos\Publicidad;
use App\Modelos\Rol;
use App\Modelos\Servicio;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\AssignOp\Concat;

class ModulosController extends Controller
{
    protected function Dashboard(){
        $datos = $this->ObtenerEstadisticasPagos();
        return view("Dashboard", compact("datos"));
    }

    protected function AltaEmpleado(){
        $roles = Rol::where('Activo', 1)->get();
        return view("Usuarios.Empleados.ABMUsuario", compact("roles"));
    }

    protected function ModificarEmpleado($idUsuario){
        $roles = Rol::where('Activo', 1)->get();
        $usuario = User::where('IdUsuario', $idUsuario)->with('Empleado')->first();

        return view("Usuarios.Empleados.ABMUsuario", compact("roles", "usuario"));
    }

    protected function ListadoEmpleados(){
        $empleados = User::where('IdTipoUsuario', 3)->with('Empleado')->get();
        return view("Usuarios.Empleados.ListadoEmpleados", compact("empleados"));
    }

    protected function ListadoClientes(){
        $clientes = User::where('IdTipoUsuario', 1)->with('PersonaFisica')->get();
        return view("Usuarios.Clientes.ListadoClientes", compact("clientes"));
    }

    protected function ListadoServicios(){
        $servicios = Servicio::all();
        return view("Servicios.ListadoServicios", compact("servicios"));
    }

    protected function ListadoMetodosDePago(){
        $metodos = MedioDePago::all();
        return view("MetodosDePago.Listado", compact("metodos"));
    }

    protected function ListadoPermisos(){
        $roles = Rol::where('Activo', 1)->get();
        $permisos = Permiso::orderBy('Permiso')->get();
        return view("Permisos.ListadoPermisos", compact("roles", "permisos"));
    }

    protected function ListadoPublicidad(){
        $publicidades = Publicidad::all();
        $arrayPublicidades = array();
        foreach ($publicidades as $key => $publicidad) {
            $colorDesactivado   = '';
            $funcionEditarFecha = "AbrirEditarFecha('{$publicidad->IdPublicidad}','{$publicidad->FechaRegistro}','{$publicidad->FechaVencimiento}')";
            $funcionAnular      = "DesactivarPublicidad('{$publicidad->IdPublicidad}')";
            if(!$publicidad->Activo){
                $colorDesactivado   = 'style="color: #f2f3f4"';
                $funcionEditarFecha = "";
                $funcionAnular      = "";
            }
            $textoBotones       = '<a href="javascript:void(0);" onclick="' . $funcionEditarFecha . '" class="text-secondary mr-2"><i class="fas fa-user-edit fa-2x"></i></a>';
            $textoBotones       .= '<a href="javascript:void(0);" onclick="' . $funcionAnular .'" class="text-secondary"><i class="fas fa-ban fa-2x"></i></a>';
            $textoBotones       = ($publicidad->Activo) ? $textoBotones : "";
            array_push($arrayPublicidades, array(
                'RutaImagen'    => '<img src="' . $publicidad->RutaImagen .'" alt="prueba" width="180px;">',
                'Descripcion'   => $publicidad->Descripcion,
                'Duracion'      => date("d/m/Y", strtotime($publicidad->FechaRegistro)) . " - " . date("d/m/Y",strtotime($publicidad->FechaVencimiento)),
                'Activo'        => $publicidad->Activo,
                'Botones'       =>  $textoBotones
            ));
        }
        return view("Publicidad.Listado",['publicidades' => $arrayPublicidades]);
    }

    protected function AltaProducto(){
        return view("Productos.ABMProductos");
    }

    protected function ModificarProducto($idProducto){
        $producto = ProductoCatalogo::where('IdProductoCatalogo', $idProducto)->first();

        return view("Productos.ABMProductos", compact("producto"));
    }

    protected function ListadoProductos(){
        $productos = ProductoCatalogo::all();
        return view("Productos.Listado", compact('productos'));
    }
    protected function ListadoMonedas(){
        $monedas = Moneda::all();
        return view("Monedas.ListadoMonedas", compact("monedas"));
    }

    protected function ListarPagos(){
        $pagos = Pago::all();
        return view("Pagos.ListadoPagos", compact('pagos'));
    }




    private function ObtenerEstadisticasPagos(){
        $pagosTotales = Pago::all();
        $pagosAnulados = Pago::Where('Anulado', 1)->get();
        $monedasUtilizadas = DB::table('Pagos')
                        ->select('IdMoneda', DB::raw('count(*) as total'))
                        ->groupBy('IdMoneda')
                        ->orderBy(DB::raw('count(*)'), 'desc')
                        ->get();

        $moneda = Moneda::where('IdMoneda', $monedasUtilizadas[0]->IdMoneda)->first();
        $usuariosRegistrados = User::where('IdTipoUsuario', 1)->where('FechaRegistro', Carbon::now())->get();
        $datos = array(
            'totalPagos'        => $pagosTotales->count(),
            'pagosAnulados'     => $pagosAnulados->count(),
            'monedaPopular'     => $moneda->Nombre,
            'nuevosUsuarios'    => $usuariosRegistrados->count(),
            'ServiciosPupulares'=> Pago::select(DB::raw('count(*) as total'), DB::raw("CONCAT(servicios.Nombre, ' - ' ,servicios.Descripcion) As Servicio"))
                                    ->join("servicios", "servicios.IdServicio", "=", "pagos.IdServicio")
                                    ->groupBy("pagos.IdServicio")
                                    ->take(10)
                                    ->get(),
            'ProductosPopulares'=> CanjeProducto::select(DB::raw('count(*) as total'),"productoscatalogo.Nombre")
                                    ->join("productoscatalogo", "productoscatalogo.IdProductoCatalogo", "=", "CanjeUsuariosProductos.IdProductoCatalogo")
                                    ->groupBy("CanjeUsuariosProductos.IdProductoCatalogo")
                                    ->take(10)
                                    ->get(),
        );

        return $datos;
    }

}
