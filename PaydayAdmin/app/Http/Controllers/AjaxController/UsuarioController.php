<?php

namespace App\Http\Controllers\AjaxController;

use App\Http\Controllers\Controller;
use App\Modelos\PersonaEmpleado;
use App\User;
use App\Utilidades\Constantes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UsuarioController extends Controller
{
    protected function AltaEmpleado(Request $request){
        $datos = $request->all();
        $this->ValidarDatosEmpleado($datos);
        if ($this->CrearUsuarioEmpleado($datos) != null){
            return response()->json(['respuesta' => 'Alta de usuario exitosa.'], 200);
        }else{
            return response()->json(['respuesta' => 'No se pudo realizar el alta.'], 500);
        }
    }

    protected function ModificarEmpleado(Request $request){
        $datos = $request->all();
        $this->ValidarDatosEmpleadoMenosEmail($datos);
        if ($this->ObtenerYActualizarEmpleado($datos)){
            return response()->json(['respuesta' => 'Usuario Modificado.'], 200);
        }else{
            return response()->json(['respuesta' => 'El correo asignado ya pertenece a otro usuario.'], 200);
        }
        
    }

    protected function ValidarDatosEmpleado($datos){
        return Validator::make($datos, [
            'email' => ['required', 'unique:Usuarios,Email','email:rfc,dns'],
            'nombre' => ['required'],
            'apellido' => ['required'],
            'documento' => ['required', 'numeric'],
            'sexo' => ['required'],
            'fechaNacimiento' => ['required'],
            'rol' => ['required'],
        ])->validate(); 
    }

    protected function ValidarDatosEmpleadoMenosEmail($datos){
        return Validator::make($datos, [
            'email' => ['required'],
            'nombre' => ['required'],
            'apellido' => ['required'],
            'documento' => ['required'],
            'sexo' => ['required'],
            'fechaNacimiento' => ['required'],
            'rol' => ['required'],
        ])->validate(); 
    }

    private function ValidarCorreoModificar(User $usuario, string $correo){
        if ($correo != ""){
            $user = User::where('Email', $correo)->where('IdUsuario', '!=', $usuario->IdUsuario)->get();
            if ($user->count() > 0){
                return false; //Mas de un correo igual
            }
            else{
                return true; //No se encontraron otro correo
            }
        }
        else{
            return false;
        }
    }

    protected function CrearUsuarioEmpleado(array $datos){
        $usuario = User::create([
            'IdTipoUsuario' => Constantes::PersonaEmpleado,
            'Email' => $datos['email'],
            'Password' =>  Hash::make('WelcomePayDay2020'),
            'FechaRegistro' => Carbon::today(),
            'Puntos'=> 0,
            'ApiToken' => Str::random(100),
            'Activo' => 1,
        ]);

        $this->GenerarPersonaEmpleado($usuario->IdUsuario, $datos);

        return $usuario;
    }

    protected function ObtenerYActualizarEmpleado(array $datos){
        $usuario = User::with('Empleado')->find($datos['idUsuario']);
        if ($this->ValidarCorreoModificar($usuario, $datos['email'])){
            $usuario->Email = $datos['email'];
            $usuario->Empleado->Nombre = $datos['nombre'];
            $usuario->Empleado->Apellido = $datos['apellido'];
            $usuario->Empleado->Documento = $datos['documento'];
            $usuario->Empleado->FechaNacimiento = $datos['fechaNacimiento'];
            $usuario->Activo = ($datos['activo']) ? 1 : 0 ;
    
            $usuario->Empleado->save();
            $usuario->save();
            return true;
        }
        return false;
    }

    public function GenerarPersonaEmpleado(int $idUsuario,array $datos){
        $empleado = PersonaEmpleado::create([
            'IdUsuario' => $idUsuario,
            'Nombre' => $datos['nombre'],
            'Apellido' => $datos['apellido'],
            'Documento' => $datos['documento'],
            'Sexo' => $datos['sexo'],
            'FechaNacimiento' => $datos['fechaNacimiento'],
            'IdRol' => $datos['rol'],
        ]);
    }
}
