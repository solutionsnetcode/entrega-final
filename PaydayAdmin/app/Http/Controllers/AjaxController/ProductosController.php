<?php

namespace App\Http\Controllers\AjaxController;

use App\Http\Controllers\Controller;
use App\Modelos\ProductoCatalogo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProductosController extends Controller
{
    protected function AltaProducto(Request $request){
        $datos = $request->all();
        if ($this->ValidarProducto($datos)){
            if ($this->CargarProducto($datos) != null){
                return response()->json(['respuesta' => 'Alta de producto exitosa.'], 200);
            }else{
                return response()->json(['respuesta' => 'Problema al cargar producto.'], 403);
            }
        }
        return response()->json(['respuesta' => 'Los datos recibidos no son validos.'], 400);
    }


    private function ValidarProducto(array $datos){
        return Validator::make($datos, [
            'nombre' => ['required'],
            'descripcion' => ['required'],
            'puntos' => ['required', 'numeric'],
        ])->validate();
    }

    private function CargarProducto(array $datos){
        $producto = ProductoCatalogo::create([
            'Nombre' => $datos['nombre'],
            'Descripcion' => $datos['descripcion'],
            'CostoPuntos' =>  $datos['puntos'],
            'RutaImagen' => '',
            'Activo' => 1,
        ]);

        $archivo = $datos['fileImagenName'];
        $extencion = Str::after($archivo->getClientOriginalName(), '.' );
        $nombreArchivo = $producto->IdProductoCatalogo . "." . $extencion;

        $archivo->move(public_path() . '/img/productos/', $nombreArchivo);

        $producto->RutaImagen = 'img/productos/' . $nombreArchivo;
        $producto->save();

        return $producto;
    }
}
