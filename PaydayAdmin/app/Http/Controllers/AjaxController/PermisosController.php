<?php

namespace App\Http\Controllers\AjaxController;

use App\Http\Controllers\Controller;
use App\Modelos\Permiso;
use App\Modelos\PermisoRol;
use App\Modelos\Rol;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PermisosController extends Controller
{
    protected function ListarPermisos(Request $request){
        $permisos = Permiso::join('permisosderoles', 'permisos.IdPermiso', '=', 'permisosderoles.IdPermiso')
        ->where('permisosderoles.IdRol', $request->idRol)
        ->get();

        return $permisos->toJson();
    }

    protected function ActualizarPermisos(Request $request){
        $datos = $request->all();

        PermisoRol::where('IdRol', $datos['rol'])->delete();

        foreach ($datos['permisos'] as $key => $value) {
            DB::table('PermisosDeRoles')->insert([
                'IdRol' => $datos['rol'],
                'IdPermiso' => $value,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }

        return response()->json(['respuesta' => 'Permisos Actulizados'], 200);
    }
}
