<?php

namespace App\Http\Controllers\AjaxController;

use App\Http\Controllers\Controller;
use App\Modelos\Pago;
use App\User;
use Illuminate\Http\Request;

class PagosController extends Controller
{
    protected function BajaPago(Request $request){
        $pago = Pago::find($request->input('pago'));

        if($pago != null){
            if($this->RestarPuntos($pago)){
                $pago->Anulado = 1;
                $pago->save();
                return response()->json(['respuesta' => 'Pago anulado'], 200);
            }
            return response()->json(['respuesta' => 'Usuario no encontrado'], 500);
        }

        return response()->json(['respuesta' => 'Pago no encontrado'], 404);
    }


    function RestarPuntos(Pago $pago){
        $usuario = User::find($pago->IdUsuario);
        if($usuario != null){
            $usuario->Puntos = $usuario->Puntos - $pago->PuntosGenerados;
            $usuario->save();
            return true;
        }

        return false;
    }
}
