<?php

namespace App\Http\Controllers\AjaxController;

use App\Http\Controllers\Controller;
use App\Modelos\Publicidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PublicidadController extends Controller
{
    protected function AltaPublicidad(Request $request){
        $datos = $request->all();
        $this->ValidarImagen($datos);
        $this->ValidarPublicidad($datos);
        $this->CargarPublicidad($datos);
        return $this->ArmarArrayPublicidad(Publicidad::all());
    }

    protected function BajaPublicidad(Request $request){
        $publicidad = Publicidad::find($request->input('publicidad'));
        $publicidad->Activo = 0;
        $publicidad->save();

        return $this->ArmarArrayPublicidad(Publicidad::all());
    }

    protected function ModificarPublicidad(Request $request){
        $datos = $request->all();
        $publicidad = Publicidad::find($datos['idPublicidad']);
        if ($publicidad != null){
            $publicidad->FechaRegistro = $datos['fechaInicio'];
            $publicidad->FechaVencimiento = $datos['fechaFin'];
            $publicidad->save();
        }

        return $this->ArmarArrayPublicidad(Publicidad::all());
    }

    private function ValidarImagen(array $datos){
        return Validator::make($datos,[
            'fileImagenName' => ['required','mimes:jpg,png,bmp,jpeg,PNG','dimensions:max_width=1800,max_height=600']
        ])->validate();
    }

    private function ValidarPublicidad(array $datos){
        return Validator::make($datos,[
            'descripcion' => ['required'],
            'fechaInicio' => ['required'],
            'fechaFin' => ['required'],
        ])->validate();
    }

    private function CargarPublicidad($data){
        $publicidad = Publicidad::create([
            'Descripcion' => $data['descripcion'],
            'RutaImagen' => '',
            'Activo' => 1,
            'FechaInicio' => $data['fechaInicio'],
            'FechaVencimiento' => $data['fechaFin']
        ]);

        $archivo = $data['fileImagenName'];
        $extencion = Str::after($archivo->getClientOriginalName(), '.' );
        $nombreArchivo = $publicidad->IdPublicidad . "." . $extencion;

        $archivo->move(public_path() . '/img/publicidad/', $nombreArchivo);

        $publicidad->RutaImagen = 'img/publicidad/' . $nombreArchivo;
        $publicidad->save();
    }



    private function ArmarArrayPublicidad($publicidades){
        $arrayPublicidades = array();
        foreach ($publicidades as $key => $publicidad) {
            $colorDesactivado   = '';
            $activo = '<span class="text-green">Activo</span>';
            $funcionEditarFecha = "AbrirEditarFecha('{$publicidad->IdPublicidad}','{$publicidad->FechaRegistro}','{$publicidad->FechaVencimiento}')";
            $funcionAnular      = "DesactivarPublicidad('{$publicidad->IdPublicidad}')";
            if($publicidad->Activo == 0){
                $colorDesactivado   = 'style="color: #f2f3f4"';
                $activo = '<span class="text-danger">Inactivo</span>';
                $funcionEditarFecha = "";
                $funcionAnular      = "";
            }

            $textoBotones       = '<a href="javascript:void(0)" onclick="' . $funcionEditarFecha . '" class="text-secondary mr-2"><i class="fas fa-user-edit fa-2x"></i></a>';
            $textoBotones       .= '<a href="javascript:void(0)" onclick="' . $funcionAnular .'" class="text-secondary"><i class="fas fa-ban fa-2x"></i></a>';
            $textoBotones       = ($publicidad->Activo) ? $textoBotones : "";
            array_push($arrayPublicidades, array(
                'RutaImagen'    => '<img src="' . $publicidad->RutaImagen .'" width="180px;"/>',
                'Descripcion'   => $publicidad->Descripcion,
                'Duracion'      => date("d/m/Y", strtotime($publicidad->FechaRegistro)) . " - " . date("d/m/Y",strtotime($publicidad->FechaVencimiento)),
                'Activo'        => $activo,
                'Botones'       =>  $textoBotones
            ));
        }

        return $arrayPublicidades;
    }

}
