<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TablaMonedasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $monedas = array(
            "1" => array(
                'nombre' => 'Pesos Uruguayos',
                'simbolo' => '$',
                'activo' => 1,
                'porcentajePuntos' => 0.01,
            ),
            "2" => array(
                'nombre' => 'Dolar Estadounidense',
                'simbolo' => 'U$S',
                'activo' => 1,
                'porcentajePuntos' => 0.5,
            ),
        );

        foreach ($monedas as $key => $moneda) {
            DB::table('Monedas')->insert([
             'Nombre' => $moneda['nombre'],
             'Simbolo' => $moneda['simbolo'],
             'Activo' => $moneda['activo'],
             'PorcentajePuntos' => $moneda['porcentajePuntos'],
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
             'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        } 
    }
}
