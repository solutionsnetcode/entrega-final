<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TablaPermisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permisos = array(
            "1" => array(
                'permiso' => 'Alta Empleado',
                'uri' => '/agregarEmpleado',
            ),
            "2" => array(
                'permiso' => 'Listar Empleado',
                'uri' => '/listadoEmpleados',
            ),
            "3" => array(
                'permiso' => 'Editar Empleado',
                'uri' => '/modificarEmpleado',
            ),
            "4" => array(
                'permiso' => 'Listar Clientes',
                'uri' => '/listadoClientes',
            ),
            "5" => array(
                'permiso' => 'Modificar Clientes',
                'uri' => '/',
            ),
            "6" => array(
                'permiso' => 'Listar Servicios',
                'uri' => '/listadoServicios',
            ),
            "7" => array(
                'permiso' => 'Alta Servicio',
                'uri' => '/ABMServiciosAjax',
            ),
            "8" => array(
                'permiso' => 'Modificar Servicio',
                'uri' => '/ABMServiciosAjax',
            ),
            "9" => array(
                'permiso' => 'Listar Medio de Pago',
                'uri' => '/listadoMetodos',
            ),
            "10" => array(
                'permiso' => 'Alta Medio de Pago',
                'uri' => '/ABMMetodosDePago',
            ),
            "11" => array(
                'permiso' => 'Modificar Medio de Pago',
                'uri' => '/ABMMetodosDePago',
            ),
            "12" => array(
                'permiso' => 'Listado de Pagos',
                'uri' => '/listadoPagos',
            ),
            "13" => array(
                'permiso' => 'Listar Pagos',
                'uri' => '/listadoPagos',
            ),
            "14" => array(
                'permiso' => 'Anular Pago',
                'uri' => '/anularPago',
            ),
            "15" => array(
                'permiso' => 'Alta Rol',
                'uri' => '/ABMRolAjax',
            ),
            "16" => array(
                'permiso' => 'Modificacion de Rol',
                'uri' => '/ABMRolAjax',
            ),
            "17" => array(
                'permiso' => 'Listar Roles',
                'uri' => '/listadoRoles',
            ),
            "18" => array(
                'permiso' => 'Lista de Publicidades',
                'uri' => '/listadoPublicidad',
            ),
            "19" => array(
                'permiso' => 'Alta de Publicidad',
                'uri' => '/altaPublicidad',
            ),
            "20" => array(
                'permiso' => 'Modificacion de Publicidad',
                'uri' => '/bajaPublicidad',
            ),
            "21" => array(
                'permiso' => 'Listar Productos del Catalogo',
                'uri' => '/listadoCatalogo',
            ),
            "22" => array(
                'permiso' => 'Alta Producto Catalogo',
                'uri' => '/altaProductoCatalogo',
            ),
            "23" => array(
                'permiso' => 'Modificacion Producto Catalogo',
                'uri' => '/modificacionProductoCatalogo',
            ),
            "24" => array(
                'permiso' => 'Listar Monedas',
                'uri' => '/listadoMonedas',
            ),
            "25" => array(
                'permiso' => 'Modificar Monedas',
                'uri' => '/modificarMonedas',
            ),
            "26" => array(
                'permiso' => 'Permisos',
                'uri' => '/permisos',
            ),

        );

        foreach ($permisos as $key => $permiso) {
            DB::table('Permisos')->insert([
             'Permiso' => $permiso['permiso'],
             'Uri' => $permiso['uri'],
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
             'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }


    }
}
