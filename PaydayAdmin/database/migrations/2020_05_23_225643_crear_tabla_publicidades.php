<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaPublicidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Publicidades', function (Blueprint $table) {
            $table->increments("IdPublicidad");
            $table->string("Descripcion");
            $table->string("RutaImagen");
            $table->boolean("Activo");
            $table->date("FechaInicio");
            $table->date("FechaVencimiento");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Publicidades');
    }
}
