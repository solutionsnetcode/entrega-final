<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaProductosCatalogo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ProductosCatalogo', function (Blueprint $table) {
            $table->increments("IdProductoCatalogo");
            $table->string("Nombre", 50);
            $table->string("Descripcion", 250);
            $table->unsignedMediumInteger("CostoPuntos");
            $table->string("RutaImagen", 250);
            $table->boolean("Activo");
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ProductosCatalogo');
    }
}
