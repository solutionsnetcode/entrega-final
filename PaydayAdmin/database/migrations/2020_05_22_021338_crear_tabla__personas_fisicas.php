<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaPersonasFisicas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         ////////////////////////////////////////////////////////////
        //   TABLA QUE CONTIENE LAS PERSONAS FISICAS DEL SISTEMA   //
        ////////////////////////////////////////////////////////////
        Schema::create('PersonasFisicas', function (Blueprint $table) {
            $table->increments("IdPersonaFisica");
            $table->unsignedInteger("IdUsuario")->unique();
            $table->string("Nombre", 50);
            $table->string("Apellido", 50);
            $table->string("Documento", 15)->unique();
            $table->enum("Sexo", ["Masculino", "Femenino", "Otro"]);
            $table->date("FechaNacimiento");
            $table->timestamps();

            
            ///////////////////
            //CLAVES FORANEAS//
            ///////////////////
            $table->foreign('IdUsuario', 'FK_Usuario_PersonaFisica')->references('IdUsuario')->on('Usuarios')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PersonasFisicas');
    }
}
