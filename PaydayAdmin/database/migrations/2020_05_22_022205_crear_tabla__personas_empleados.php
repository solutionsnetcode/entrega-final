<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaPersonasEmpleados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PersonasEmpleados', function (Blueprint $table) {
            $table->increments("IdEmpleado");
            $table->unsignedInteger("IdUsuario")->unique();
            $table->string("Nombre", 50);
            $table->string("Apellido", 50);
            $table->string("Documento", 15)->unique();
            $table->enum("Sexo", ["Masculino", "Femenino", "Otro"]);
            $table->date("FechaNacimiento");
            $table->unsignedTinyInteger("IdRol");
            $table->timestamps();

            ///////////////////
            //CLAVES FORANEAS//
            ///////////////////
            $table->foreign('IdUsuario', 'FK_Usuario_PersonaEmpleado')->references('IdUsuario')->on('Usuarios')->onDelete('restrict');
            $table->foreign('IdRol', 'FK_Rol_PersonaEmpleado')->references('IdRol')->on('Roles')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PersonasEmpleados');
    }
}
