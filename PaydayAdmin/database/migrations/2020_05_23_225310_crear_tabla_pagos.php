<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaPagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pagos', function (Blueprint $table) {
            $table->increments("IdPago");
            $table->unsignedInteger("IdUsuario");
            $table->unsignedTinyInteger("IdMoneda");
            $table->decimal("MontoPago", 10,2);
            $table->unsignedInteger("PuntosGenerados")->default(0);
            $table->unsignedInteger("IdServicio");
            $table->date("FechaGenera");
            $table->date("FechaPaga");
            $table->unsignedTinyInteger("IdMedioPago");
            $table->boolean("Anulado")->default(false);
            $table->timestamps();

            ///////////////////
            //CLAVES FORANEAS//
            ///////////////////
            $table->foreign('IdUsuario', 'FK_Usuario_Pago')->references('IdUsuario')->on('Usuarios')->onDelete('restrict');
            $table->foreign('IdMoneda', 'FK_Moneda_Pago')->references('IdMoneda')->on('Monedas')->onDelete('restrict');
            $table->foreign('IdServicio', 'FK_Servicio_Pago')->references('IdServicio')->on('Servicios')->onDelete('restrict');
            $table->foreign('IdMedioPago', 'FK_MedioPago_Pago')->references('IdMedioPago')->on('MediosDePago')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Pagos');
    }
}
