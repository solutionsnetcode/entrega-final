<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\PagosController;
use App\Http\Controllers\ProductosController;
use App\Http\Controllers\PublicidadController;
use App\Http\Controllers\RecuperarContraseñaController;
use App\Http\Controllers\UsuarioController;
use App\Models\Publicidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


route::get('HashContraseña', function(){
    //echo Hash::make("Prueba123");
});


///////////////////////////////////////////////////////////////////
///////////////////      RUTAS PUBLICAS      /////////////////////
//////////////////////////////////////////////////////////////////

Route::get('login', [AuthController::class, 'IniciarSesion']);
Route::post('user', [AuthController::class, 'Registro']);

//-> RUTAS PARA RECUPERAR CONTRASEÑA
Route::group(['prefix' => 'recovery'], function () {
    // Envia un correo con un token que permite acceder al cambio de contraseña (Preferible usar esta)
    Route::post('', [RecuperarContraseñaController::class, 'RecuperarContraseña']);

    // Recibe un token y si este es correcto nos envia a la ventana de cambio de contraseña
    Route::get('/{token}', [RecuperarContraseñaController::class, 'RecuperarContraseñaView'])->name('recuperarContraseña');

    // Realiza el cambio de contraseña del usuario
    Route::put('/{token}', [RecuperarContraseñaController::class, 'CambioDeContraseña']);
});


Route::get('publicidades', [PublicidadController::class, 'ObtenerPublicidadesActivas']);

###################################################################
###################################################################




////////////////////////////////////////////////////////////////////
////////////////////   RUTAS SOLO AUTENTICADOS ////////////////////
///////////////////////////////////////////////////////////////////

Route::group(['middleware' => 'auth:api'], function() {

    //-> RUTAS DE USUARIO #####
    Route::group(['prefix' => 'user'], function () {
        // Devuelve una lista de usuarios o un usuario en caso de que se le pase un parametro (id de usuario)
        Route::get('', [UsuarioController::class, 'ObtenerUsuarios']);

        // Modifica lo datos de un usuario creado
        Route::put('', [UsuarioController::class, 'ModificacionUsuario']);

        // Permite eliminar un usuario del sistema (Baja logica)
        Route::delete('', [UsuarioController::class, 'BajaUsuario']);

        // Permite obtener los servicios agendados
        Route::get('/services', [UsuarioController::class, 'ServiciosAgendados']);

        //Permite obtener los metodos de pago
        Route::get('paymentMethods', [UsuarioController::class, 'MetodosDePagos']);

    });

    //-> RUTAS DE PAGO ######
    Route::group(['prefix' => 'payments'], function () {
        // Pagos Realizados
        Route::get('', [PagosController::class, "AgregarPago"]);

        // Registra un pago en la plataforma
        Route::post('', [PagosController::class, "AgregarPago"]);
    });


    //-> RUTAS DE PRODUCTOS
    Route::group(['prefix' => 'products'], function () {
        Route::get('/{id?}', [ProductosController::class, 'Productos']);

        Route::post('', [ProductosController::class, 'CanjearProducto']);
    });

    Route::group(['prefix' => 'services'], function () {
        Route::get('/{id?}', [ProductosController::class, 'Servicios']);
        Route::post('', [ProductosController::class, 'AgendarServicio']);
    });

});
Route::get('datosPago', [PagosController::class, 'PrepararPago']);










// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
