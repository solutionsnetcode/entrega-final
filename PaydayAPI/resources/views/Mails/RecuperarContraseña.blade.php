<br/>
<table style="max-width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;">
    <tr>
        <td style="background-color: #ecf0f1; text-align: left; padding: 0">
            <a href="">
                <img width="100%" style="display:block; margin-top: 2%" />
            </a>
        </td>
    </tr>

    <tr>
        <td style="background-color: #ecf0f1">
            <div style="color: #34495e; margin: 4% 10% 2%; text-align: justify; font-family: sans-serif">
                <h2 style="color: #e67e22; margin: 0 0 7px">¡Recupera tu cuenta!</h2>
                <p style="margin: 2px; font-size: 15px;">
                    Has solicitado una recuperacion de contraseña.
                </p>
                <br/>
                <p style="margin: 2px; font-size: 15px;">
                    De parte del equipo de Payday te facilitamos el siguiente enlace para poder cambiar tu contraseña de manera facil y rapida
                </p>
                <p style="margin: 2px; font-size: 15px;">
                    Estamos siempre trabajando para brindarte seguridad y mejores tiempos en tus transacciones.
                </p>

                <br/>

                <p style="margin: 2px; font-size: 15px;">
                    Podras cambiar la contraseña de tu cuenta haciendo click en el boton verde:
                </p>
                <br/>
                <br/>
                <div style="width: 100%; text-align: center">
                    <a style="text-decoration: none; border-radius: 5px; padding: 11px 23px; color: white; background-color: #02be69" href="{{'http://localhost:8080/recuperarContraseña/' . $tokenActivacion}}">
                        Cambiar contraseña
                    </a>
                </div>
                <br/>
                <br/>
                <center>
                    <p style="margin: 5px; font-size: 13px; color: #b3b3b3;">
                        ¿No funciona el boton? Prueba con este enlace:
                    </p>
                    <a style="color: #b3b3b3; font-size: 13px; text-align: center; margin: 30px 0 0" href="{{'http://localhost:8080/recuperarContraseña/' . $tokenActivacion}}">
                        {{'http://localhost:8080/recuperarContraseña/' . $tokenActivacion}}
                    </a>
                    <br/>
                    <br/>
                    <p style="margin: 5px; font-size: 13px; color: #b3b3b3;">
                       Payday 2020
                    </p>
                </center>
            </div>
        </td>
    </tr>
</table>
<br/>
