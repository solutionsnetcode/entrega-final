<br/>
<table style="max-width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;">
    <tr>
        <td style="background-color: #ecf0f1; text-align: left; padding: 0">
            <a href="">
                <img width="100%" style="display:block; margin-top: 2%" />
            </a>
        </td>
    </tr>

    <tr>
        <td style="background-color: #ecf0f1">
            <div style="color: #34495e; margin: 4% 10% 2%; text-align: justify; font-family: sans-serif">
                <h2 style="color: #e67e22; margin: 0 0 7px">¡Pago Realizado!</h2>
                <p style="margin: 2px; font-size: 15px;">
                    {{$pago->Usuario->DatosUsuario->Nombre . " " . $pago->Usuario->DatosUsuario->Apellido . ", has realizado un pago por " . $pago->Moneda->Simbolo . " " . $pago->MontoPago}}
                </p>
                <br/>
                <p style="margin: 2px; font-size: 15px;">
                    ¡Tu pago genero {{$pago->PuntosGenerados}} Puntos!
                </p>
                <p style="margin: 2px; font-size: 15px;">
                    No olivdes que puedes cambiar tus puntos por una variedad de productos.
                </p>

                <br/>

                <p style="margin: 2px; font-size: 15px;">
                    Estamos siempre trabajando para brindarte seguridad y mejores tiempos en tus transacciones.
                </p>
                <center>
                    <p style="margin: 5px; font-size: 13px; color: #b3b3b3;">
                       Payday 2020
                    </p>
                </center>
            </div>
        </td>
    </tr>
</table>
<br/>

