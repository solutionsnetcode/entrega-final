<?php

namespace App\Http\Controllers;

use App\Models\Publicidad;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PublicidadController extends Controller
{
    protected function ObtenerPublicidadesActivas(){
        $fecha = Carbon::now();

        $publicidades = Publicidad::where('Activo', 1)->where('FechaInicio', '<=', $fecha->format('Y-m-d'))->where('FechaVencimiento', '>=', $fecha->format('Y-m-d'))->get()->pluck('RutaImagen');
        $publicidades = $publicidades->toJson();
        return response()->json($publicidades, 200);
    }
}
