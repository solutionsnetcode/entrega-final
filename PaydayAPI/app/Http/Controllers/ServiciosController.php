<?php

namespace App\Http\Controllers;

use App\Models\Servicio;
use Illuminate\Http\Request;

class ServiciosController extends Controller
{
    protected function ObtenerServicios(){
        $servicios = Servicio::where('Activo', 1)->get()->pluck('Nombre');
        return response()->json(['servicios' => $servicios]);
    }
}
