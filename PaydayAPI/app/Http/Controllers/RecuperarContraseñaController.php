<?php

namespace App\Http\Controllers;

use App\Mail\RecuperarContraseña as MailRecuperarContraseña;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class RecuperarContraseñaController extends Controller
{
    public function RecuperarContraseña(Request $request){
        if ($this->ValidarExistenciaUsuario($request->email)){
            $this->EnviarMailRecuperarContraseña($request->email);
            return response()->json(['respuesta' => 'Correo enviado para cambiar la contraseña.'], 200);
        }
        else{
            return response()->json(['respuesta' => 'El usuario no existe.'], 500);
        }
    }

    public function CambioDeContraseña(Request $request, $token){
        $datos = $request->all();
        if ($datos['contraseña1'] == $datos['contraseña2']){
            $usuario = User::where('TokenActivacion', $token)->whereNotNull('TokenActivacion')->first();
            if ($usuario != null){
                $usuario->Password = Hash::make($datos['contraseña1']);
                $usuario->TokenActivacion = NULL;
                $usuario->save();
                return response()->json(['respuesta' => 'Contraseña cambiada correctamente']);
            }
            return response()->json(['respuesta' => 'Token Expirado'], 404);
        }
        return response()->json(['respuesta' => 'Contraseñas no coinciden'], 404);
    }

    public function RecuperarContraseñaView($token){
        if (User::where('TokenActivacion', $token)->first() != null){
            return response()->json(['respuesta' => 'Token valido']);
        }
        else{
            return response()->json(['respuesta' => 'Token Expirado'], 404);
        }
    }

    private function ValidarExistenciaUsuario($email){
        $existe = false;
        $usuario = User::where('Email', $email)->first();
        if ($usuario != null){
            $existe = true;
            $usuario->TokenActivacion = Str::random(40);
            $usuario->save();
        }

        return $existe;
    }

    private function EnviarMailRecuperarContraseña(string $email){
        $usuario = User::where('Email', $email)->first();
        Mail::to($usuario->Email)->send(New MailRecuperarContraseña($usuario->TokenActivacion));
    }
}
