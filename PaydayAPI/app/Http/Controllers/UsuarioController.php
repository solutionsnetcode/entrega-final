<?php

namespace App\Http\Controllers;

use App\Models\Pago;
use App\Models\PersonaFisica;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsuarioController extends Controller
{
    protected function ObtenerUsuarios(){
        $usu = auth('api')->user();
        if ($usu != null){
            $usu->DatosUsuario;
            $pagos = Pago::where('IdUsuario', $usu->IdUsuario)->orderBy('created_at', 'DESC')->get();

            $pagosArray = $this->ArmarArrayPagos($pagos);
            return response()->json(['usuario' => $usu, 'pagos' => $pagosArray]);
        }
        else{
            return response()->json(['usuario' => null]);
        }
    }

    protected function ModificacionUsuario(Request $request){
        $datosModificar = $request->all();
        $this->ValidarDatosRegistro($datosModificar);
        $usu = auth('api')->user();

        $usuarioModificar = User::find($usu->IdUsuario);

        if ($usuarioModificar != null){
            if($this->ValidarCorreoModificar($usuarioModificar, $datosModificar['email'])){
                $usuarioModificar->Email = $datosModificar['email'];
                $coinciden = Hash::check($datosModificar['password'], $usu->Password);
                if($datosModificar['password'] != ""){
                    if (!$coinciden){
                        return response()->json(['respuesta' => 'La contraseña anterior no coincide'], 404);
                    }else{
                        if(strlen($datosModificar['password2']) < 8){
                            return response()->json(['respuesta' => 'La contraseña nueva debe tener una longtud de 8 caracteres.'], 404);
                        }else{
                            $usuarioModificar->Password = Hash::make($datosModificar['password2']);
                        }
                    }
                }
                $personaFisica = PersonaFisica::where('IdUsuario', $usu->IdUsuario)->first();
                $personaFisica->Sexo = $datosModificar['sexo'];
                $personaFisica->Nombre = $datosModificar['nombre'];
                $personaFisica->Apellido = $datosModificar['apellido'];
                $usuarioModificar->save();
                $personaFisica->save();
                return response()->json(['respuesta' => 'Usuario modificado correctamente.'], 200);
            }
            else{
                return response()->json(['respuesta' => 'El nuevo correo ya pertenece a otro usuario.'], 500);
            }
        }

        else{
            return response()->json(['respuesta' => 'El usuario a modificar no existe.'], 500);
        }

    }

    public function BajaUsuario(Request $request){
        $datos = $request->all();
        $usuario = User::find($datos['idUsuario']);

        if($usuario != null){
            $usuario->Activo = 0;
            $usuario->save();
            return response()->json(['respuesta' => 'Se realizo la baja del usuario.'], 200);
        }
        else{
            return response()->json(['respuesta' => 'El usuario no existe.'], 200);
        }
    }


    private function ArmarArrayPagos($pagos){
        $arrayP = array();
        foreach ($pagos as $key => $pago) {
          $datos =  array(
                'nombre' => $pago->Servicio->Nombre,
                'monto' => $pago->Moneda->Simbolo . " " . $pago->MontoPago,
                'puntos' => $pago->PuntosGenerados,
                'fecha' => Carbon::parse($pago->FechaGenera)->format('d/m/Y'),
                'estado' => ($pago->Anulado == 1) ? 'Anulado' : 'Realizado',
            );

            array_push($arrayP, $datos);
        }

        return $arrayP;
    }

    private function ValidarDatosRegistro(array $datos){
        $validacion = Validator::make($datos, [
            'email' => ['required', 'unique:Usuarios,Email','email:rfc,dns'],
            'password' => ['required', 'alpha_num'],
            'nombre' => ['required'],
            'apellido' => ['required'],
            'documento' => ['required', 'numeric', 'unique:PersonasFisicas,Documento'],
            'sexo' => ['required'],
        ]);
        return $validacion->errors();
    }

    private function ValidarCorreoModificar(User $usuario, string $correo){
        if ($correo != ""){
            $user = User::where('Email', $correo)->where('IdUsuario', '!=', $usuario->IdUsuario)->get();
            if ($user->count() > 0){
                return false; //Mas de un correo igual
            }
            else{
                return true; //No se encontraron otro correo
            }
        }
        else{
            return false;
        }
    }
}
