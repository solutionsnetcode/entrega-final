<?php

namespace App\Http\Controllers;

use App\Mail\ConfirmacionPago;
use App\Models\MedioDePago;
use App\Models\Moneda;
use App\Models\Pago;
use App\Models\Servicio;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class PagosController extends Controller
{
    protected function AgregarPago(Request $request){
        $datos = $this->ArmarArray($request->all());
        $this->ValidarDatosPago($datos);
        $pago = $this->AltaPago($datos);
        //es porque se rechazan de manera random.
        if($pago->Anulado == 0){
            $this->AumentarPuntaje($pago);
            $this->NotificarPago($pago);
            return response()->json(['respuesta' => 'Pago realizado. Verifica tu correo.']);
        }else{
            return response()->json(['respuesta' => "Pago rechazado."], 404);
        }

    }

    protected function PrepararPago(){
        $monedas =  Moneda::where('Activo',1)->get()->pluck('Simbolo');
        $servicios = Servicio::where('Activo', 1)->get()->pluck('Nombre');
        return response()->json(['servicios' => $servicios, 'monedas' => $monedas]);
    }



    private function ValidarDatosPago(array $datos){
        $validacion = Validator::make($datos, [
            'monto' => ['required', 'numeric'],
            'medioPago' => ['required', 'numeric', 'exists:MediosDePago,IdMedioPago'],
            'moneda' => ['required', 'numeric', 'exists:Monedas,IdMoneda'],
            'servicio' => ['required', 'numeric', 'exists:Servicios,IdServicio'],
            'usuario' => ['required', 'exists:Usuarios,IdUsuario'],
            'numeroTarjeta' => ['required'],
            'ccv' => ['required'],
        ]);

        if($validacion->fails()){
            return response()->json($validacion->errors(), 403);
        }
        // if ($validacion->errors()->count() > 0){
        //     return response()->json(['errores' => $validacion->errors()]);
        // }
    }

    private function AltaPago(array $datos){
        $usuario = User::find($datos['usuario']);
        return Pago::create([
            'IdUsuario' => $usuario->IdUsuario,
            'IdMoneda' => $datos['moneda'],
            'MontoPago' =>  $datos['monto'],
            'IdServicio' => $datos['servicio'],
            'FechaGenera'=> Carbon::today(),
            'FechaPaga' => Carbon::today(),
            'PuntosGenerados' => Moneda::find($datos['moneda'])->PorcentajePuntos * $datos['monto'],
            'IdMedioPago' => $datos['medioPago'],
            'Anulado' => rand(0,1),
        ]);
    }

    private function AumentarPuntaje(Pago $pago){
        $usuario = User::find($pago->IdUsuario);
        $usuario->Puntos += $pago->PuntajeGanado();
        $usuario->save();
    }

    private function NotificarPago(Pago $pago){
        Mail::to($pago->Usuario->Email)->send(new ConfirmacionPago($pago));
    }


    private function ArmarArray($datos){
        $moneda = Moneda::where('Simbolo', $datos['moneda'])->first();
        $servicio = Servicio::where('Nombre', $datos['servicio'])->first();
        $usuario = auth('api')->user();
        return array(
            'monto' => $datos['monto'],
            'medioPago' => 1,
            'moneda' => $moneda->IdMoneda,
            'servicio' => $servicio->IdServicio,
            'usuario' => $usuario->IdUsuario,
            'numeroTarjeta' => '',
            'ccv' => '',
        );
    }
}
