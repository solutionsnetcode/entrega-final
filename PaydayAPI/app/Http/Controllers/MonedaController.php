<?php

namespace App\Http\Controllers;

use App\Models\Moneda;
use Illuminate\Http\Request;

class MonedaController extends Controller
{
    protected function ObtenerMonedas(){
        $monedas Moneda::where('Activo',1)->get()->pluck('Simbolo');
        return response()->json(['monedas' => $monedas])
    }
}
