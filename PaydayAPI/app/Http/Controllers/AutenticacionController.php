<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AutenticacionController extends Controller
{


    public function CerrarSesion(Request $request){
        $request->user()->token()->revoke();

        return response()->json([
            'respuesta' => 'Se desconecto correctamente',
            200
        ]);
    }
}
