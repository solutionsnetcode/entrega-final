<?php

namespace App\Http\Controllers;

use App\Models\PersonaFisica;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function IniciarSesion(Request $request){
        $usuario = User::where('Email', $request->email)->where('IdTipoUsuario', 1)->first();
        if($usuario != null){
            if (Hash::check($request->password, $usuario->Password)){
                $tokenResult = $usuario->createToken('Personal Access Token');
                $token = $tokenResult->token;
                $token->save();
                return response()->json([
                    'token'         => $tokenResult->accessToken,
                    'usuario'       => $usuario->DatosUsuario->Nombre,
                    'tipo_token'    => 'Bearer',
                    'expira'        => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
                ]);
            }
            else{
                return response()->json(['respuesta' => 'Credenciales erroneas.'], 401);
            }
        }
        else{
            return response()->json(['respuesta' => 'Credenciales erroneas.'], 404);
        }
    }

    /**
     * Registro de usuario
     */
    public function Registro(Request $request){
        $datos = $request->all();
        $errores = $this->ValidarDatosRegistro($datos);

        if (!empty($errores->messages)){
            return response()->json(['respuesta' => 'Error al realizar el registro.', 'errores' => $errores], 402);
        }

        $this->RegistrarNuevoUsuario($datos);
        return response()->json(['respuesta' => 'Usuario registrado correctamente.', 'errores' => $errores], 201);
    }

    public function ModificarUsuario(Request $request){
        $datosModificar = $request->all();
        $errores = $this->ValidarDatosRegistro($datosModificar);

        if(!empty($errores)){
            return response()->json(['respuesta' => 'Error al modificar usuario.', 'errores' => $errores], 402);
        }

        $usuarioModificar = User::where('Email',$datosModificar['usuario'])->where('ApiToken', $datosModificar['token'])->first();

        if ($usuarioModificar != null){
            if($this->ValidarCorreoModificar($usuarioModificar, $datosModificar['email'])){
                $usuarioModificar->Email = $datosModificar['email'];
                $usuarioModificar->Password = $datosModificar['password'];
                $usuarioModificar->save();
                return response()->json(['respuesta' => 'Usuario modificado correctamente.', 'errores' => $errores], 200);
            }
            else{
                return response()->json(['respuesta' => 'El nuevo correo ya pertenece a otro usuario.', 'errores' => $errores], 402);
            }
        }
        else{
            return response()->json(['respuesta' => 'El usuario a modificar no existe.'], 404);
        }
    }

    private function RegistrarNuevoUsuario(array $datos){
        $usuario = User::create([
            'IdTipoUsuario' => 1,
            'Email' => $datos['email'],
            'Password' =>  Hash::make($datos['password']),
            'FechaRegistro' => Carbon::today(),
            'Puntos'=> 0,
            'Activo' => 1,
        ]);

        $this->CrearPersonaFisica($usuario->IdUsuario, $datos);

        return $usuario;
    }

    private function CrearPersonaFisica(int $idUsuario, array $datos){
        PersonaFisica::create([
            'IdUsuario' => $idUsuario,
            'Nombre' => $datos['nombre'],
            'Apellido' => $datos['apellido'],
            'Documento' => $datos['documento'],
            'Sexo' => $datos['sexo'],
            'FechaNacimiento'=> $datos['fechaNacimiento'],
        ]);
    }

    private function ValidarDatosRegistro(array $datos){
        $validacion = Validator::make($datos, [
            'email' => ['required', 'unique:Usuarios,Email','email:rfc,dns'],
            'password' => ['required', 'alpha_num'],
            'nombre' => ['required'],
            'apellido' => ['required'],
            'documento' => ['required', 'numeric', 'unique:PersonasFisicas,Documento'],
            'sexo' => ['required'],
            'fechaNacimiento' => ['required'],
        ]);

        return $validacion->errors();
    }

    private function ValidarUsuario(Request $request){
        return $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string'
        ]);
    }

    private function ValidarCorreoModificar(User $usuario, string $correo){
        if ($correo != ""){
            $user = User::where('Email', $correo)->where('IdUsuarioR', '!=', $usuario->IdUsuarioR)->get();
            if ($user->count() > 0){
                return false; //Mas de un correo igual
            }
            else{
                return true; //No se encontraron otro correo
            }
        }
        else{
            return false;
        }
    }
}
