<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    protected $table = 'Usuarios';
    protected $primaryKey = 'IdUsuario';
    protected $remember_token = true;
    protected $guarded = ['IdUsuario'];


    protected $fillable = [
        'IdTipoUsuario', 'Email', 'Password', 'FechaRegistro','TokenActivacion', 'rememberToken', 'Puntos', 'Activo', 'ApiToken'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getAuthPassword()
    {
        $pass = $this->Password;
        return $pass;
    }


    public function DatosUsuario(){
            return $this->hasOne(PersonaFisica::class, 'IdUsuario' ,'IdUsuario');
    }

}
