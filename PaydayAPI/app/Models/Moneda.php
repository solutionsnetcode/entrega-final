<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Moneda extends Model
{
    use HasFactory;

    protected $table = 'Monedas';
    protected $primaryKey = 'IdMoneda';

    protected $fillable = [
        'Nombre', 'Simbolo', 'Activo', 'PorcentajePuntos'
    ];
}
