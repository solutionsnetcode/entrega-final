<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Publicidad extends Model
{
    use HasFactory;

    protected $table = 'Publicidades';
    protected $primaryKey = 'IdPublicidad';

    protected $fillable = [
        'Descripcion', 'RutaImagen', 'Activo', 'FechaRegistro', 'FechaVencimiento'
    ];
}
