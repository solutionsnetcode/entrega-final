<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductoCatalogo extends Model
{
    use HasFactory;

    protected $table = 'ProductosCatalogo';
    protected $primaryKey = 'IdProductoCatalogo';

    protected $fillable = [
        'Nombre', 'Descripcion', 'CostoPuntos', 'RutaImagen', 'Activo'
    ];
}
