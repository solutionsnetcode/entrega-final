<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CanjeProducto extends Model
{
    use HasFactory;

    protected $table = 'CanjeUsuariosProductos';
    protected $primaryKey = 'IdCanje';

    protected $fillable = [
        'IdUsuario', 'IdProductoCatalogo', 'FechaRealizado', 'Anulado'
    ];
}
