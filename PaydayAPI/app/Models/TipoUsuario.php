<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoUsuario extends Model
{
    use HasFactory;

    protected $table = 'TiposUsuario';
    protected $primaryKey = 'Id';

    protected $fillable = [
        'TipoUsuario', 'Activo'
    ];
}
